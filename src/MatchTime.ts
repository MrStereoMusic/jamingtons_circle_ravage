import { AnhaCoreEvent } from './../lib/AnhaCore/Events/AnhaCoreEvent';
import { Canvas } from '../lib/AnhaCore/Rendering/Shared/Canvas';
import { EventArgs } from '../lib/AnhaCore/Events/EventArgs';
import { Image } from '../lib/AnhaCore/UI/Image';
import { jcrGame } from './Main/JCR';
import { List } from '../lib/AnhaCore/Collection/List';
import { MathTool } from '../lib/AnhaCore/Tool/MathTool';
import { Scene } from '../lib/AnhaCore/Scene/Scene';
import { SceneObject } from '../lib/AnhaCore/Scene/SceneObject';
import { Timer } from '../lib/AnhaCore/Timing/Timer';
import { Vector2 } from '../lib/AnhaCore/Physics/Vector2';
import { Vector2Config } from '../lib/AnhaCore/Config/Vector2Config';

export class MatchTime extends SceneObject {

    private image: Image;
    private totalBarWidth: number;

    private time: number;
    private timer: Timer;

    public onTimeOut: AnhaCoreEvent<any>;

    constructor(image: Image, position: Vector2Config, time: number, stage: Canvas) {
        super();

        this.image = image;
        this.totalBarWidth = this.image.transform.width;
        this.time = time;

        this.onTimeOut = new AnhaCoreEvent<any>();

        this.timer = new Timer({
            interval: time,
            action: () => {
                this.onTimeOut.dispatch(this, null);
                this.timer.stop();
                this.image.transform.width = 0;
            }
        });
    }

    public start(): void {
        this.timer.start();
    }

    public reset(): void {
        this.timer.reset();
        this.timer.start();
    }

    public update(d: number): void {
        this.timer.update(d);

        let percents = (this.timer.currentTime / this.time);

        if (percents > 0) {
            this.image.transform.width = this.totalBarWidth - (this.totalBarWidth * percents);
        }

    }

    public render(stage: Canvas): void {
        // Nothing to render here. All objects were added to the scene before.
    }
}