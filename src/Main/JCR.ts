import { AnhaCoreGame } from '../../lib/AnhaCore/Core/AnhaCoreGame';
import { EndMatch } from '../Scenes/EndMatch';
import { InGame } from '../Scenes/InGame';
import { Intro } from '../Scenes/Intro';
import { JCRConfig } from './JCR.config';
import { Keyboard } from '../../lib/AnhaCore/Input/Keyboard';
import { MainMenu } from '../Scenes/MainMenu';
import { Mouse } from '../../lib/AnhaCore/Input/Mouse';
import { Pause } from '../Scenes/Pause';

class JCR extends AnhaCoreGame {

    constructor() {

        super(JCRConfig, [
            new InGame(),
            new Pause(),
            new MainMenu(),
            new EndMatch(),
            new Intro()
        ]);

        this.input.addDevice<Keyboard>(new Keyboard(this));
        this.input.addDevice<Mouse>(new Mouse(this.client));
    }

}

export let jcrGame = new JCR();

