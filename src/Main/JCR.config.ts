export let JCRConfig = {
    gameName: "Jamingtons Circle Ravage!",
    resolutionX: 1024,
    resolutionY: 576,
    initSceneCode: "Intro",
    developerMode: false,
    imageSmoothing: false,
    enableWebGL: false,
    images: {
        sprites: "assets/images/sprites.png",
        spriteFont: "assets/images/spritefont.png"
    },
    developer: "Yvo Geldhof",
    copyright: "Jamingtons Circle Ravage!, July 2017, All rights reserved.",
    version: "v1.0.0"
};