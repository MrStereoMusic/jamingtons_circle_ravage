import { Component } from '../../lib/AnhaCore/Component/Component';
import { DrawTrackComponentConfig } from './DrawTrackComponentConfig';
import { List } from '../../lib/AnhaCore/Collection/List';
import { Vector2Config } from '../../lib/AnhaCore/Config/Vector2Config';

export class DrawTrackComponent extends Component {

    public readonly color: Array<number>;
    public readonly thickness: number;
    public readonly delay: number;
    public readonly coordinates: Array<any>;

    constructor(config: DrawTrackComponentConfig) {
        super("drawTrackComponent");

        this.color = config.color;
        this.thickness = config.thickness;
        this.coordinates = [];
        this.delay = config.delay;
    }

    public addCoord(x: number, y: number): boolean {
        let last = this.coordinates[this.coordinates.length - 1];

        if (last) {

            if (Math.abs(last.x - x) < this.delay) {
                return false;
            }
        }

        this.coordinates.push({
            x: x, 
            y: y,
            accumulator: 0
        });

        return true;
    }

    public clearCoords(): void {
        this.coordinates.length = 0;
    }
}