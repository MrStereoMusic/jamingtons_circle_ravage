import { Circle } from '../../lib/AnhaCore/Component/UI/Circle';
import { Entity } from '../../lib/AnhaCore/World/Entity';
import { jcrGame } from '../Main/JCR';
import { Origin } from '../../lib/AnhaCore/Component/UI/Origin.enum';
import { RGBA } from '../../lib/AnhaCore/UI/RGBA';
import { Transform2D } from '../../lib/AnhaCore/Component/UI/Transform2D';
import { TweenAnimations } from '../../lib/AnhaCore/Component/Animation/Tweening/TweenAnimations';
import { World } from '../../lib/AnhaCore/World/World';
 
export class CircleEntity extends Entity {

    public radius: number;

    constructor(world: World, color: Array<number>, radius: number) {
        super();

        let stage = jcrGame.client.stage;

        this.radius = radius;

        let transform = <Transform2D>this.addComponent(new Transform2D({
            width: radius * 2,
            height: radius * 2,
            position: {
                x: 0,
                y: 0
            },
            angle: 0,
            zIndex: -1
        }));

        transform.setOrigin(Origin.CENTER);

        this.addComponent(new TweenAnimations({}));

        this.addComponent(new Circle({
            color: color, // TODO: dynamically, based on kart color.
            radius: radius, // TODO: dynamically, based on the drawn radius.
            stage: stage
        }));
    }
}