import { Color } from '../../lib/AnhaCore/UI/Color';

export type DrawTrackComponentConfig = {
    color: Array<number>;
    thickness: number;
    delay: number;
};