import { CircleEntity } from './CircleEntity';
import { DrawTrackComponent } from './DrawTrackComponent';
import { Kart } from '../Kart';
import { Origin } from '../../lib/AnhaCore/Component/UI/Origin.enum';
import { RGBA } from '../../lib/AnhaCore/UI/RGBA';
import { System } from '../../lib/AnhaCore/System/System';
import { Transform2D } from '../../lib/AnhaCore/Component/UI/Transform2D';
import { Transitions } from '../../lib/AnhaCore/Animation/Tweening/Transition/Transitions';
import { TweenAnimations } from '../../lib/AnhaCore/Component/Animation/Tweening/TweenAnimations';
import { Vector2Config } from '../../lib/AnhaCore/Config/Vector2Config';
import { World } from '../../lib/AnhaCore/World/World';
import { WorldEventArgs } from '../../lib/AnhaCore/World/WorldEventArgs';

export class DrawTrackSystem extends System {

    private msDecay: number;
    private drawnRadius: number;
    private drawnCentroid: Vector2Config;
    private previousDrawnCircle: CircleEntity;

    constructor() {
        super({
            tag: "drawTrackSystem",
            requiredComponents: ["drawTrackComponent"]
        });

        this.msDecay = 3;
        this.drawnRadius = 0;
        this.drawnCentroid = {
            x: 0,
            y: 0
        };
    }

    public initialize(world: World): void {
        world.onPreRender.subscribe(this, this.onRender);
    }

    public preUpdateSystem(d: number, world: World): void {
        //
    }

    public updateSystem(d: number, world: World): void {
        let ents = this.entities.all();
        let i, j, len = ents.length, ent;
        let comp, compCoords;

        for (i = 0; i < len; ++i) {
            ent = ents[i];
            comp = ent.getComponent("drawTrackComponent");

            for (j = 0; j < comp.coordinates.length; ++j) {
                compCoords = comp.coordinates[j];

                compCoords.accumulator += d;

                if (compCoords.accumulator > this.msDecay) {
                    comp.coordinates.splice(j, 1);
                }
            }

            // is it a circle?
            if (this.validateUserDrawnThingy(comp, world)) {

                if (this.previousDrawnCircle) {

                    let previousCircleTransform = this.previousDrawnCircle.getComponent<Transform2D>("transform2d");
                    let dx = previousCircleTransform.position.x - this.drawnCentroid.x;
                    let dy = previousCircleTransform.position.y - this.drawnCentroid.y;
                    let distance = Math.sqrt(dx * dx + dy * dy);

                    // circles may not overlap
                    if (distance < (this.previousDrawnCircle.radius + this.drawnRadius)) {
                        this.spawnCircle(world, RGBA.RED, 0.25, 0.25, comp);

                        return;
                    }
                }

                this.previousDrawnCircle = this.spawnCircle(world, RGBA.LIGHTBLUE, 2, 1, comp);

                // 1.2 = multiplier, 50 = points_per_circle
                let points = (1.2 * this.drawnRadius) + 50;
                (<Kart>ent).addScore(points);
            }
        }
    }

    private spawnCircle(world: World, color: Array<number>, duration: number, delay: number, comp: DrawTrackComponent): CircleEntity {

        let circle = new CircleEntity(world, color, this.drawnRadius);
        let circleTransform = circle.getComponent<Transform2D>("transform2d");

        circleTransform.setOrigin(Origin.CENTER);

        circleTransform.position.x = this.drawnCentroid.x;
        circleTransform.position.y = this.drawnCentroid.y;

        let animations = circle.getComponent<TweenAnimations>("tweenAnimations");

        animations.tweens.add({
            properties: [
                {
                    from: 1,
                    to: 0,
                    change: (to: number) => {
                        circleTransform.effects.opacity = to;
                    }
                }
            ],
            callback: () => {
                world.removeEntity(circle);
            },
            duration: duration,
            delay: delay,
            easing: Transitions.ease
        });

        comp.clearCoords();

        world.addEntity(circle);

        return circle;
    }

    private validateUserDrawnThingy(comp: DrawTrackComponent, world: World): boolean {

        // add a little bit more accuracy..
        if (comp.coordinates.length < 10) {
            return false;
        }

        let sumX = 0;
        let sumY = 0;
        let total = comp.coordinates.length;
        let a;

        for (a = 0; a < total; ++a) {
            sumX += comp.coordinates[a].x;
            sumY += comp.coordinates[a].y;
        }

        let minY = Math.min.apply(Math, comp.coordinates.map(function (o) { return o.y; }));

        let averageX = sumX / total;
        let averageY = sumY / total;
        let radius = (sumY / total) - minY;

        // GET THE PERCENT INSIDE THE CIRCLE

        // compute distance to approximated center from each point
        let distances = comp.coordinates.map(function (coord, index) {
            return Math.pow(coord.x - averageX, 2) + Math.pow(coord.y - averageY, 2);
        });

        if (distances.length > 0) {

            let minPercentInRange = 0.9;

            // average of those distance is 
            let averageDistance = distances.reduce(function (previous, current) {
                return previous + current;
            }) / distances.length;

            let min = averageDistance * minPercentInRange;
            let max = averageDistance * 1.2;

            // filter out the ones not inside the min and max boundaries 
            let inRange = distances.filter(function (s) {
                return s > min && s < max;
            });

            let percentInRange = inRange.length / total;

            // by the % of points within those boundaries we can guess if it's circle
            if (percentInRange > minPercentInRange) {

                this.drawnRadius = radius;
                this.drawnCentroid.x = averageX;
                this.drawnCentroid.y = averageY;

                return true;
            }
        }

        return false;
    }

    public postUpdateSystem(d: number, world: World): void {
        //
    }

    public onRender(sender: World, args: WorldEventArgs): void {
        let ctx = args.ctx;

        let pi = 2 * Math.PI;
        let ents = this.entities.all();
        let i, j, len = ents.length, ent;
        let comp, compCoords;

        for (i = 0; i < len; ++i) {
            ent = ents[i];

            comp = ent.getComponent("drawTrackComponent");

            for (j = 0; j < comp.coordinates.length; ++j) {
                compCoords = comp.coordinates[j];

                // TODO: do something with the newly added Circle object.
                ctx.save();
                ctx.beginPath();
                ctx.arc(compCoords.x, compCoords.y, comp.thickness, 0, pi);
                ctx.fillStyle = '#D7B753';
                ctx.fill();
                ctx.restore();
            }
        }
    }

    public reset(): void {
        this.drawnRadius = 0;
        this.drawnCentroid.x = 0;
        this.drawnCentroid.y = 0;
        this.previousDrawnCircle = null;
    }
}
