import { Component } from '../lib/AnhaCore/Component/Component';
import { DrawTrackComponent } from './World/DrawTrackComponent';
import { Kart } from './Kart';
import { Key } from '../lib/AnhaCore/Input/Key.enum';
import { Keyboard } from '../lib/AnhaCore/Input/Keyboard';
import { MathTool } from '../lib/AnhaCore/Tool/MathTool';
import { ParticleCollection } from '../lib/AnhaCore/Component/Particles/ParticleCollection';
import { ParticleEmitter } from '../lib/AnhaCore/Particles/ParticleEmitter';
import { Physics2D } from '../lib/AnhaCore/Component/Physics/Physics2D';
import { Transform2D } from '../lib/AnhaCore/Component/UI/Transform2D';
import { Transitions } from '../lib/AnhaCore/Animation/Tweening/Transition/Transitions';
import { Vector2Config } from '../lib/AnhaCore/Config/Vector2Config';

// TODO: place it in Rectangle class or so
function getCenterOfRect(x, y, width, height, angle_degrees): Vector2Config {
    var angle_rad = angle_degrees * Math.PI / 180;
    var cosa = Math.cos(angle_rad);
    var sina = Math.sin(angle_rad);
    var wp = width / 2;
    var hp = height / 2;

    return {
        x: (x + wp * cosa - hp * sina),
        y: (y + wp * sina + hp * cosa)
    };
}

export class KartInputController extends Component {

    private player: Kart;

    private physics: Physics2D;
    private transform: Transform2D;
    private slipTracker: ParticleEmitter;
    private drawTrack: DrawTrackComponent;

    private enabled: boolean;

    constructor(keyboard: Keyboard, player: Kart) {
        super("kartInputController");

        this.player = player;
        this.enabled = true;

        this.physics = this.player.getComponent<Physics2D>("physics2d");
        this.transform = this.player.getComponent<Transform2D>("transform2d");
        this.drawTrack = this.player.getComponent<DrawTrackComponent>("drawTrackComponent");
        this.slipTracker = this.player.getComponent<ParticleCollection>("particleCollection").emitter;

        keyboard.onKeyDown.subscribe(this, this.onKeyDown);
        keyboard.onKeyUp.subscribe(this, this.onKeyUp);
    }

    public onKeyDown(sender: Keyboard, args: KeyboardEvent) {

        if (!this.enabled) {
            return;
        }

        let physics = this.physics;

        if (sender.isKeyHeld(Key.W) || sender.isKeyHeld(Key.S)) {
            this.slipTracker.enable();
        } else {

            if (physics.velocity.x < 40) {
                this.slipTracker.disable();
            }
        }

        if (sender.isKeyHeld(Key.SPACE)) {
            let transform = this.transform;
            let mid = getCenterOfRect(transform.position.x, transform.position.y, transform.width, transform.height, transform.angle);

            this.drawTrack.addCoord(mid.x, mid.y);
        }

        if (sender.isKeyHeld(Key.W)) {
            physics.friction.x = 0;
            physics.acceleration.x = physics.maxAcceleration.x;
            physics.friction.y = 0;
            physics.acceleration.y = physics.maxAcceleration.y;
        }

        if (sender.isKeyHeld(Key.S)) {
            physics.friction.x = 0;
            physics.acceleration.x = -physics.maxAcceleration.x;
            physics.friction.y = 0;
            physics.acceleration.y = -physics.maxAcceleration.y;
        }

        if (sender.isKeyHeld(Key.A)) {
            this.transform.angle -= 5;
        }

        if (sender.isKeyHeld(Key.D)) {
            this.transform.angle += 5;
        }
    }

    public onKeyUp(sender: Keyboard, args: KeyboardEvent) {
        let physics = this.physics;

        if (!sender.isKeyHeld(Key.W)) {
            physics.friction.y = physics.maxFriction.y;
        }

        if (!sender.isKeyHeld(Key.S)) {
            physics.friction.y = physics.maxFriction.y;
        }

        if (!sender.isKeyHeld(Key.A)) {
            physics.friction.x = physics.maxFriction.x;
        }

        if (!sender.isKeyHeld(Key.D)) {
            physics.friction.x = physics.maxFriction.x;
        }
    }

    public enable(): void {
        this.enabled = true;
    }

    public disable(): void {
        this.enabled = false;
    }
}