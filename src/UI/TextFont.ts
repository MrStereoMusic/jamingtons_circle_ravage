import { Action } from '../../lib/AnhaCore/Core/Action';
import { Canvas } from '../../lib/AnhaCore/Rendering/Shared/Canvas';
import { Image } from '../../lib/AnhaCore/UI/Image';
import { ImageConfig } from '../../lib/AnhaCore/Config/ImageConfig';
import { Mouse } from '../../lib/AnhaCore/Input/Mouse';
import { SceneObject } from '../../lib/AnhaCore/Scene/SceneObject';
import { Vector2 } from '../../lib/AnhaCore/Physics/Vector2';
import { Vector2Config } from '../../lib/AnhaCore/Config/Vector2Config';

export class TextFont extends SceneObject {

    public text: string;
    private color: string;
    private size: number;
  
    constructor(config: any) {
        super(config.transform);

        this.text = config.text;
        this.color = config.color;
        this.size = config.size;
    }

    public update(d: number): void {
        //
    }

    public render(stage: Canvas): void {
        let ctx = <CanvasRenderingContext2D>stage.context;

        ctx.font = `${this.size}px Arial sans-serif`;
        ctx.fillStyle = this.color;
        ctx.fillText(this.text, this.transform.position.x+this.transform.translation.x, this.transform.position.y+this.transform.translation.y);
    }
}