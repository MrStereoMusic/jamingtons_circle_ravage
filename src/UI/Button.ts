import { Action } from '../../lib/AnhaCore/Core/Action';
import { Image } from '../../lib/AnhaCore/UI/Image';
import { ImageConfig } from '../../lib/AnhaCore/Config/ImageConfig';
import { Mouse } from '../../lib/AnhaCore/Input/Mouse';
import { Vector2 } from '../../lib/AnhaCore/Physics/Vector2';
import { Vector2Config } from '../../lib/AnhaCore/Config/Vector2Config';

export class Button extends Image {

    private clickCallback: Action;

    private normalFrame: Vector2Config;
    private hoverFrame: Vector2Config;

    // TODO: make this constructor with a config
    constructor(config: ImageConfig, mouse: Mouse, clickCallback: Action, hoverFrame: Vector2Config) {
        super(config);

        this.clickCallback = clickCallback;
        this.normalFrame = {
            x: this.sprite.frame.x,
            y: this.sprite.frame.y
        };
        this.hoverFrame = hoverFrame;

        mouse.onButtonDown.subscribe(this, this.onMouseButtonDown);
        mouse.onMove.subscribe(this, this.onMouseMove);
    }

    private onMouseButtonDown(sender: Mouse, args: MouseEvent): void {

        if (this.acceptInput) {
            let pos = sender.position;

            if (this.mouseHoveringMe(pos)) {
                this.clickCallback();
            }

        }
    }

    private onMouseMove(sender: Mouse, args: MouseEvent): void {

        if (this.acceptInput) {
            let pos = sender.position;

            if (this.mouseHoveringMe(pos)) {
                this.sprite.frame.x = this.hoverFrame.x;
                this.sprite.frame.y = this.hoverFrame.y;
            } else {
                this.sprite.frame.x = this.normalFrame.x;
                this.sprite.frame.y = this.normalFrame.y;
            }
        }
    }

    private mouseHoveringMe(mousePosition: Vector2): boolean {
        let transform = this.transform;

        return (
            mousePosition.x >= (transform.position.x + transform.translation.x) &&
            mousePosition.x <= (transform.position.x + transform.translation.x + transform.width) &&
            mousePosition.y >= transform.position.y + transform.translation.y&&
            mousePosition.y <= (transform.position.y + transform.translation.y + transform.height)
        );
    }
}