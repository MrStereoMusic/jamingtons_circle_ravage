import { Canvas } from '../../lib/AnhaCore/Rendering/Shared/Canvas';
import { Client } from '../../lib/AnhaCore/Core/Client';
import { Image } from '../../lib/AnhaCore/UI/Image';
import { jcrGame } from '../Main/JCR';
import { Mouse } from '../../lib/AnhaCore/Input/Mouse';
import { RGBA } from '../../lib/AnhaCore/UI/RGBA';
import { Scene } from '../../lib/AnhaCore/Scene/Scene';
import { Timer } from '../../lib/AnhaCore/Timing/Timer';
import { Transitions } from '../../lib/AnhaCore/Animation/Tweening/Transition/Transitions';

export class Intro extends Scene {

    private logo: Image;
    private footer: Image;

    constructor() {

        super({
            tag: "Intro",
            predecessorSettings: {
                pause: false
            },
            bgColor: RGBA.WHITE
        });

    }

    public onInitialize(stage: Canvas): void {
        let client: Client = jcrGame.client;
        let viewport = client.resolution;

        this.logo = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("sprites"),
                frame: {
                    x: 400,
                    y: 872
                },
                stage: stage
            },
            transform: {
                width: 460,
                height: 107,
                zIndex: 5
            }
        }));

        this.footer = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("sprites"),
                frame: {
                    x: 117,
                    y: 1063
                },
                stage: stage
            },
            transform: {
                translation: {
                    x: 0,
                    y: (stage.height / 2) - 52
                },
                width: 913,
                height: 42,
                zIndex: 5
            }
        }));

        this.logo.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);
        this.footer.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);
    }

    public onReset(): void {
        //
    }

    public onEnter(): void {

        this.transitions.add({
            properties: [{
                from: 0,
                to: 1,
                change: (to: number) => {
                    this.transform.effects.opacity = to;
                }
            }
            ],
            callback: () => {
                this.enterCompleted();
            },
            duration: 1,
            easing: Transitions.ease
        });

        this.transitions.add({
            properties: [{
                from: 1,
                to: 0,
                change: (to: number) => {
                    this.transform.effects.opacity = to;
                }
            }
            ],
            callback: () => {
                jcrGame.sceneActivator.switchToScene("MainMenu");
            },
            delay: 4,
            duration: 1,
            easing: Transitions.ease
        });
    }

    public onExit(): void {
        this.exitCompleted();
    }

    public onResume(): void {
        //
    }

    public onOverlap(scene: Scene): void {
        //
    }
}