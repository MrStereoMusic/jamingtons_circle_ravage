import { Canvas } from '../../lib/AnhaCore/Rendering/Shared/Canvas';
import { Client } from '../../lib/AnhaCore/Core/Client';
import { Image } from '../../lib/AnhaCore/UI/Image';
import { jcrGame } from '../Main/JCR';
import { Mouse } from '../../lib/AnhaCore/Input/Mouse';
import { RGBA } from '../../lib/AnhaCore/UI/RGBA';
import { Scene } from '../../lib/AnhaCore/Scene/Scene';
import { Transitions } from '../../lib/AnhaCore/Animation/Tweening/Transition/Transitions';

export class Pause extends Scene {

    private logo: Image;
    private mouse: Mouse;

    constructor() {

        super({
            tag: "Pause",
            predecessorSettings: {
                pause: true
            },
            bgColor: RGBA.TRANSPARENT
        });

    }

    public onInitialize(stage: Canvas): void {
        let client: Client = jcrGame.client;
        let viewport = client.resolution;

        this.mouse = jcrGame.input.getDevice<Mouse>("mouse");

        this.logo = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 14,
                    y: 14
                },
                stage: stage
            },
            transform: {
                effects: {
                    opacity: 0
                },
                position: {
                    x: 0,
                    y: 0
                },
                width: 304,
                height: 80,
                zIndex: 2
            }
        }));

        this.logo.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);
        this.mouse.onButtonDown.subscribe(this, this.onMouseButtonDown);
    }

    public onReset(): void {
        this.logo.transform.effects.opacity = 0;
        this.logo.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        this.mouse.onButtonDown.subscribe(this, this.onMouseButtonDown);
    }

    private onMouseButtonDown(sender: Mouse, args: MouseEvent): void {

        if (this.acceptInput) {
            jcrGame.sceneActivator.popScene();

            let mouse = jcrGame.input.getDevice<Mouse>("mouse");
            mouse.onButtonDown.unsubscribe(this);
        }

    }

    public onEnter(): void {

        this.transitions.add({
            properties: [
                {
                    from: -100,
                    to: 100,
                    change: (to: number) => {
                        this.logo.transform.position.y = to;
                    }
                }, {
                    from: 0,
                    to: 1,
                    change: (to: number) => {
                        this.logo.transform.effects.opacity = to;
                    }
                }
            ],
            callback: () => {
                this.enterCompleted();
            },
            delay: 0.2,
            duration: 0.7,
            easing: Transitions.bounceOut
        });
    }

    public onExit(): void {

        this.transitions.add({
            properties: [
                {
                    from: 100,
                    to: -100,
                    change: (to: number) => {
                        this.logo.transform.position.y = to;
                    }
                }, {
                    from: 1,
                    to: 0,
                    change: (to: number) => {
                        this.logo.transform.effects.opacity = to;
                    }
                }
            ],
            callback: () => {
                this.exitCompleted();
            },
            duration: 0.5,
            easing: Transitions.ease
        });

    }

    public onResume(): void {
        //
    }

    public onOverlap(scene: Scene): void {
        //
    }
}