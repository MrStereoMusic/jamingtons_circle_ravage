import { Button } from '../UI/Button';
import { Canvas } from '../../lib/AnhaCore/Rendering/Shared/Canvas';
import { Client } from '../../lib/AnhaCore/Core/Client';
import { Image } from '../../lib/AnhaCore/UI/Image';
import { jcrGame } from '../Main/JCR';
import { Mouse } from '../../lib/AnhaCore/Input/Mouse';
import { RGBA } from '../../lib/AnhaCore/UI/RGBA';
import { Scene } from '../../lib/AnhaCore/Scene/Scene';
import { TextFont } from '../UI/TextFont';
import { Transitions } from '../../lib/AnhaCore/Animation/Tweening/Transition/Transitions';

export class MainMenu extends Scene {

    private logo: Image;
    private startButton: Button;
    private background: Image;

    constructor() {

        super({
            tag: "MainMenu",
            predecessorSettings: {
                pause: false
            },
            bgColor: RGBA.WHITE
        });

    }

    public onInitialize(stage: Canvas): void {
        let client: Client = jcrGame.client;
        let viewport = client.resolution;

        this.background = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("sprites"),
                frame: {
                    x: 121,
                    y: 0
                },
                stage: stage
            },
            transform: {
                position: {
                    x: 0,
                    y: 0
                },
                width: stage.width,
                height: stage.height,
                zIndex: -1
            }
        }));

        let highscoreImage = this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 517,
                    y: 24
                },
                stage: stage
            },
            transform: {
                position: {
                    x: 0,
                    y: 0
                },
                translation: {
                    x: -40,
                    y: 250
                },
                width: 140,
                height: 39,
                zIndex: 3
            }
        }));

        highscoreImage.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        let highscoreValue = "0";
        if (jcrGame.data.hasData()) {
            highscoreValue = jcrGame.data.getSavePart("highscore").value;
        }

        let highscoreText = this.add(new TextFont({
            text: highscoreValue || "not available",
            color: "#000",
            size: 35,
            transform: {
                position: {
                    x: 0,
                    y: 0
                },
                translation: {
                    x: 40,
                    y: 258
                },
                zIndex: 3
            }
        }));

        highscoreText.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        this.startButton = <Button>this.add(new Button({
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 513,
                    y: 80
                },
                stage: stage
            },
            transform: {
                position: {
                    x: 0,
                    y: 0
                },
                translation: {
                    x: 0,
                    y: 80
                },
                width: 160,
                height: 50,
                zIndex: 2
            }
        }, jcrGame.input.getDevice<Mouse>("mouse"), () => {
            jcrGame.sceneActivator.switchToScene("InGame");
        }, {
                x: 513,
                y: 146
            }));

        this.startButton.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        this.logo = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 0,
                    y: 372
                },
                stage: stage
            },
            transform: {
                effects: {
                    opacity: 0
                },
                position: {
                    x: 0,
                    y: 0
                },
                width: 708,
                height: 230,
                zIndex: 1
            }
        }));

        this.logo.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);
    }

    public onReset(): void {
        //
    }

    public onEnter(): void {

        this.transitions.add({
            properties: [{
                from: 0,
                to: 1,
                change: (to: number) => {
                    this.transform.effects.opacity = to;
                }
            }
            ],
            duration: 0.5,
            easing: Transitions.ease
        });

        this.transitions.add({
            properties: [
                {
                    from: -100,
                    to: 50,
                    change: (to: number) => {
                        this.logo.transform.position.y = to;
                    }
                }, {
                    from: 0,
                    to: 1,
                    change: (to: number) => {
                        this.logo.transform.effects.opacity = to;
                    }
                }
            ],
            callback: () => {
                this.enterCompleted();
            },
            delay: 0.6,
            duration: 0.7,
            easing: Transitions.bounceOut
        });
    }

    public onExit(): void {

        this.transitions.add({
            properties: [
                {
                    from: 50,
                    to: 0,
                    change: (to: number) => {
                        this.logo.transform.position.y = to;
                    }
                }, {
                    from: 1,
                    to: 0,
                    change: (to: number) => {
                        this.logo.transform.effects.opacity = to;
                    }
                }
            ],
            duration: 0.5,
            easing: Transitions.ease
        });

        this.transitions.add({
            properties: [{
                from: 1,
                to: 0,
                change: (to: number) => {
                    this.transform.effects.opacity = to;
                }
            }
            ],
            callback: () => {
                this.exitCompleted();
            },
            duration: 0.5,
            easing: Transitions.ease
        });
    }

    public onResume(): void {
        //
    }

    public onOverlap(scene: Scene): void {
        //
    }
}