import { Button } from '../UI/Button';
import { Canvas } from '../../lib/AnhaCore/Rendering/Shared/Canvas';
import { Client } from '../../lib/AnhaCore/Core/Client';
import { Image } from '../../lib/AnhaCore/UI/Image';
import { jcrGame } from '../Main/JCR';
import { Mouse } from '../../lib/AnhaCore/Input/Mouse';
import { RGBA } from '../../lib/AnhaCore/UI/RGBA';
import { Scene } from '../../lib/AnhaCore/Scene/Scene';
import { TextFont } from '../UI/TextFont';
import { Transitions } from '../../lib/AnhaCore/Animation/Tweening/Transition/Transitions';

export class EndMatch extends Scene {

    private logo: Image;
    private mouse: Mouse;
    private background: Image;

    private mainMenuButton: Button;
    private playAgainButton: Button;

    constructor() {

        super({
            tag: "EndMatch",
            predecessorSettings: {
                pause: true
            },
            bgColor: RGBA.TRANSPARENT
        });

    }

    public onInitialize(stage: Canvas, data: { [key: string]: any }): void {
        let client: Client = jcrGame.client;
        let viewport = client.resolution;

        this.logo = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 0,
                    y: 372
                },
                stage: stage
            },
            transform: {
                effects: {
                    opacity: 0
                },
                position: {
                    x: 0,
                    y: 0
                },
                width: 708,
                height: 230,
                zIndex: 3
            }
        }));

        this.logo.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        this.background = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("sprites"),
                frame: {
                    x: 122,
                    y: 739
                },
                stage: stage
            },
            transform: {
                translation: {
                    x: 0,
                    y: 100
                },
                width: 261,
                height: 311,
                zIndex: 1
            }
        }));

        this.background.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        this.playAgainButton = <Button>this.add(new Button({
            sprite: {
                image: jcrGame.imageLoader.getImage("sprites"),
                frame: {
                    x: 55,
                    y: 162
                },
                stage: stage
            },
            transform: {
                translation: {
                    x: 50,
                    y: 200
                },
                width: 52,
                height: 52,
                zIndex: 2
            }
        }, jcrGame.input.getDevice<Mouse>("mouse"), () => {

            jcrGame.sceneActivator.popScene({
                reset: true
            });

        }, { x: 55, y: 216 }));

        this.mainMenuButton = <Button>this.add(new Button({
            sprite: {
                image: jcrGame.imageLoader.getImage("sprites"),
                frame: {
                    x: 0,
                    y: 162
                },
                stage: stage
            },
            transform: {
                translation: {
                    x: -50,
                    y: 200
                },
                width: 52,
                height: 52,
                zIndex: 2
            }
        }, jcrGame.input.getDevice<Mouse>("mouse"), () => {
            jcrGame.sceneActivator.switchToScene("MainMenu");
        }, { x: 0, y: 216 }));

        let highscoreImage = this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 517,
                    y: 24
                },
                stage: stage
            },
            transform: {
                translation: {
                    x: -40,
                    y: 0
                },
                width: 140,
                height: 39,
                zIndex: 3
            }
        }));

        highscoreImage.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        let highscoreValue = "0";
        let showScoreValue = 0;
        if (jcrGame.data.hasData()) {
            highscoreValue = jcrGame.data.getSavePart("highscore").value;

            if (highscoreValue) {
                showScoreValue = (data.score > highscoreValue ? data.score : highscoreValue);
            }
        }

        let highscoreText = this.add(new TextFont({
            text: showScoreValue,
            color: "#000",
            size: 35,
            transform: {
                translation: {
                    x: 40,
                    y: 10
                },
                zIndex: 5
            }
        }));

        highscoreText.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        let scoreImage = this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 514,
                    y: 224
                },
                stage: stage
            },
            transform: {
                translation: {
                    x: -68,
                    y: 75
                },
                width: 89,
                height: 34,
                zIndex: 3
            }
        }));

        scoreImage.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);


        let scoreText = this.add(new TextFont({
            text: data.score,
            color: "#000",
            size: 35,
            transform: {
                translation: {
                    x: -15,
                    y: 86
                },
                zIndex: 5
            }
        }));

        scoreText.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        this.mainMenuButton.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);
        this.playAgainButton.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        this.mouse = jcrGame.input.getDevice<Mouse>("mouse");

        if (highscoreValue && data && data.score > highscoreValue) {

            // save the highscore
            jcrGame.data.addSaveData("highscore", {
                value: data.score
            });

        }
    }

    public onReset(): void {
        this.mouse.onButtonDown.unsubscribe(this.mainMenuButton);
        this.mouse.onMove.unsubscribe(this.mainMenuButton);

        this.mouse.onButtonDown.unsubscribe(this.playAgainButton);
        this.mouse.onMove.unsubscribe(this.playAgainButton);
    }

    public onEnter(): void {

        this.transitions.add({
            properties: [{
                from: 0,
                to: 1,
                change: (to: number) => {
                    this.transform.effects.opacity = to;
                }
            }
            ],
            duration: 0.5,
            easing: Transitions.ease
        });

        this.transitions.add({
            properties: [
                {
                    from: -50,
                    to: 50,
                    change: (to: number) => {
                        this.logo.transform.position.y = to;
                    }
                }, {
                    from: 0,
                    to: 1,
                    change: (to: number) => {
                        this.logo.transform.effects.opacity = to;
                    }
                }
            ],
            callback: () => {
                this.enterCompleted();
            },
            delay: 0.2,
            duration: 0.7,
            easing: Transitions.bounceOut
        });
    }

    public onExit(): void {
        let stage = jcrGame.client.stage;

        this.transitions.add({
            properties: [{
                from: 1,
                to: 0,
                change: (to: number) => {
                    this.transform.effects.opacity = to;
                }
            }
            ],
            duration: 0.5,
            easing: Transitions.ease
        });

        this.transitions.add({
            properties: [
                {
                    from: 50,
                    to: -50,
                    change: (to: number) => {
                        this.logo.transform.position.y = to;
                    }
                }, {
                    from: 1,
                    to: 0,
                    change: (to: number) => {
                        this.logo.transform.effects.opacity = to;
                    }
                }
            ],
            callback: () => {
                this.exitCompleted();
            },
            duration: 0.5,
            easing: Transitions.ease
        });
    }

    public onResume(): void {
        //
    }

    public onOverlap(scene: Scene): void {
        //
    }
}