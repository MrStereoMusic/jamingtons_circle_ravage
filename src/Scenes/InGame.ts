import { Button } from '../UI/Button';
import { Camera2D } from '../../lib/AnhaCore/Core/Camera2D';
import { Canvas } from '../../lib/AnhaCore/Rendering/Shared/Canvas';
import { Client } from '../../lib/AnhaCore/Core/Client';
import { DrawTrackSystem } from '../World/DrawTrackSystem';
import { Image } from '../../lib/AnhaCore/UI/Image';
import { jcrGame } from '../Main/JCR';
import { Kart } from '../Kart';
import { KartInputController } from '../KartInputController';
import { Keyboard } from '../../lib/AnhaCore/Input/Keyboard';
import { MatchTime } from '../MatchTime';
import { Mouse } from '../../lib/AnhaCore/Input/Mouse';
import { Origin } from '../../lib/AnhaCore/Component/UI/Origin.enum';
import { Particles } from '../../lib/AnhaCore/System/Particles';
import { RGBA } from '../../lib/AnhaCore/UI/RGBA';
import { Scene } from '../../lib/AnhaCore/Scene/Scene';
import { TextFont } from '../UI/TextFont';
import { Timer } from '../../lib/AnhaCore/Timing/Timer';
import { Transform2D } from '../../lib/AnhaCore/Component/UI/Transform2D';
import { Transitions } from '../../lib/AnhaCore/Animation/Tweening/Transition/Transitions';
import { Tween } from '../../lib/AnhaCore/Animation/Tweening/Tween';
import { TweenConfig } from '../../lib/AnhaCore/Config/TweenConfig';
import { Tweening } from '../../lib/AnhaCore/System/Tweening';
import { Vector2 } from '../../lib/AnhaCore/Physics/Vector2';
import { World } from '../../lib/AnhaCore/World/World';

export class InGame extends Scene {

    private readonly MATCH_TIME_SEC = 48;

    private world: World;
    private background: Image;
    private countdownOne: Image;
    private countdownTwo: Image;
    private countdownThree: Image;
    private countdownFour: Image;
    private foregroundTimer: Image;
    private matchTimer: MatchTime;
    private pauseButton: Button;
    private player: Kart;

    private scoreValueText: TextFont;

    constructor() {

        super({
            tag: "InGame",
            predecessorSettings: {
                pause: false
            },
            bgColor: RGBA.WHITE
        });

    }

    public onInitialize(stage: Canvas): void {
        let client: Client = jcrGame.client;
        let viewport = client.resolution;

        this.pauseButton = <Button>this.add(new Button({
            sprite: {
                image: jcrGame.imageLoader.getImage("sprites"),
                frame: {
                    x: 0,
                    y: 112
                },
                stage: stage
            },
            transform: {
                position: {
                    x: stage.width - 10 - 25,
                    y: 10
                },
                width: 25,
                height: 30,
                zIndex: 2
            }
        }, jcrGame.input.getDevice<Mouse>("mouse"), () => {
            jcrGame.sceneActivator.pushScene("Pause");
        }, {
                x: 37,
                y: 112
            }));

        this.background = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("sprites"),
                frame: {
                    x: 121,
                    y: 0
                },
                stage: stage
            },
            transform: {
                position: {
                    x: 0,
                    y: 0
                },
                width: stage.width,
                height: stage.height,
                zIndex: -1
            }
        }));

        // ## START: MATCH TIMER

        let backgroundTimer = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("sprites"),
                frame: {
                    x: 401,
                    y: 743
                },
                stage: stage
            },
            transform: {
                translation: {
                    x: 0,
                    y: -258
                },
                width: 164,
                height: 40,
                zIndex: 6
            }
        }));

        this.foregroundTimer = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("sprites"),
                frame: {
                    x: 405,
                    y: 785
                },
                stage: stage
            },
            transform: {
                translation: {
                    x: 0,
                    y: -258
                },
                width: 156,
                height: 34,
                zIndex: 8
            }
        }));

        this.foregroundTimer.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);
        backgroundTimer.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);

        this.matchTimer = <MatchTime>this.add(new MatchTime(this.foregroundTimer, new Vector2(0, 0), this.MATCH_TIME_SEC, stage));
        this.matchTimer.onTimeOut.subscribe(this, this.onMatchTimeOut);

        // ## END: MATCH TIMER

        let camera = new Camera2D({
            viewport: {
                width: viewport.x,
                height: viewport.y
            },
            bounds: {
                min: {
                    x: 0,
                    y: 0
                },
                max: {
                    x: 0,
                    y: 0
                }
            },
            focus: {
                x: viewport.x * 0.5,
                y: viewport.y * 0.5
            },
            smoothness: 4.0
        });

        this.world = <World>this.add(new World({
            camera: camera,
            stage: jcrGame.client.stage,
            size: {
                x: client.resolution.x,
                y: client.resolution.y
            },
            gravity: 981,
            systems: [
                new Tweening(),
                new Particles(),
                new DrawTrackSystem()
            ]
        }));

        this.player = new Kart(this.world);
        this.player.addComponent(new KartInputController(jcrGame.input.getDevice<Keyboard>("keyboard"), this.player));
        camera.setWorldSize(this.world.size.x, this.world.size.y);
        this.world.addEntity(this.player);

        this.countdownOne = <Image>this.add(new Image({ // 3
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 321,
                    y: 121
                },
                stage: stage
            },
            transform: {
                width: 135,
                height: 250,
                zIndex: 9,
                effects: {
                    opacity: 0
                }
            }
        }));

        this.countdownTwo = <Image>this.add(new Image({ // 2
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 133,
                    y: 124
                },
                stage: stage
            },
            transform: {
                width: 145,
                height: 179,
                zIndex: 9,
                effects: {
                    opacity: 0
                }
            }
        }));

        this.countdownThree = <Image>this.add(new Image({ // 1
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 11,
                    y: 128
                },
                stage: stage
            },
            transform: {
                width: 53,
                height: 173,
                zIndex: 9,
                effects: {
                    opacity: 0
                }
            }
        }));

        this.countdownFour = <Image>this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 0,
                    y: 372
                },
                stage: stage
            },
            transform: {
                width: 708,
                height: 230,
                zIndex: 9,
                effects: {
                    opacity: 0
                }
            }
        }));

        this.countdownOne.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);
        this.countdownOne.transform.setOrigin(Origin.CENTER);

        this.countdownTwo.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);
        this.countdownTwo.transform.setOrigin(Origin.CENTER);

        this.countdownThree.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);
        this.countdownThree.transform.setOrigin(Origin.CENTER);

        this.countdownFour.transform.centerOn(this.transform.position, this.transform.width, this.transform.height);
        this.countdownFour.transform.setOrigin(Origin.CENTER);


        let scoreImage = this.add(new Image({
            sprite: {
                image: jcrGame.imageLoader.getImage("spriteFont"),
                frame: {
                    x: 514,
                    y: 224
                },
                stage: stage
            },
            transform: {
                position: {
                    x: 10,
                    y: 10
                },
                width: 89,
                height: 34,
                zIndex: 3
            }
        }));

        this.scoreValueText = <TextFont>this.add(new TextFont({
            text: "0000000000",
            color: "#000",
            size: 30,
            transform: {
                position: {
                    x: 110,
                    y: 37
                },
                zIndex: 8
            }
        }));

        this.player.onCircleDrawn.subscribe(this, this.onPlayerCircleDrawn);


    }

    public onEnter(): void {

        this.transform.effects.opacity = 0;

        this.transitions.add({
            properties: [
                {
                    from: 0,
                    to: 1,
                    change: (to: number) => {
                        this.transform.effects.opacity = to;
                    }
                }
            ],
            duration: 0.5,
            delay: 0.4,
            callback: () => {
                this.onMatchStart(true);
            },
            easing: Transitions.ease
        });
    }

    public onExit(): void {
        this.exitCompleted();
    }

    public onResume(data: { [key: string]: any }): void {

        this.transitions.add({
            properties: [
                {
                    from: 0.2,
                    to: 1,
                    change: (to: number) => {
                        this.transform.effects.opacity = to;
                    }
                }
            ],
            duration: 0.25,
            callback: () => {

                if (data && data.reset) {
                    this.onReset();
                }

                this.resumeCompleted();

            },
            easing: Transitions.ease
        });

    }

    public onOverlap(scene: Scene): void {

        this.transitions.add({
            properties: [
                {
                    from: 1,
                    to: 0.2,
                    change: (to: number) => {
                        this.transform.effects.opacity = to;
                    }
                }
            ],
            duration: 0.25,
            callback: () => {
                this.overlapCompleted(scene);
            },
            easing: Transitions.ease
        });

    }

    private getCountdownTweenConfig(transform: Transform2D, delay: number, isEnter: boolean, last: boolean): void {
        let duration;
        let endDuration;
        let waitBeforeOut = delay + duration;
        let cb;

        if (last) {
            duration = 0.5;
            endDuration = 0.3;
            waitBeforeOut = delay + duration + 0.1;

            cb = () => {

                if (isEnter) {
                    this.enterCompleted();
                } else {
                    this.resumeCompleted();
                }

                this.matchTimer.start();
            };

        } else {
            duration = 0.6;
            endDuration = 0.4;
            waitBeforeOut = delay + duration;
        }

        this.transitions.add({
            init: () => {
                this.disableInput();
            },
            properties: [
                {
                    from: 0,
                    to: 1,
                    change: (to: number) => {
                        transform.scale.x = transform.scale.y = to;
                    }
                }, {
                    from: 0,
                    to: 1,
                    change: (to: number) => {
                        transform.effects.opacity = to;
                    }
                }
            ],
            delay: delay,
            duration: duration,
            easing: Transitions.ease
        });

        this.transitions.add({
            init: () => {
                this.disableInput();
            },
            properties: [
                {
                    from: 1,
                    to: 3,
                    change: (to: number) => {
                        transform.scale.x = transform.scale.y = to;
                    }
                }, {
                    from: 1,
                    to: 0,
                    change: (to: number) => {
                        transform.effects.opacity = to;
                    }
                }
            ],
            delay: waitBeforeOut,
            duration: endDuration,
            easing: Transitions.ease,
            callback: cb
        });
    }

    public onReset(): void {
        let sceneTransform = this.transform;

        this.disableInput();

        this.scoreValueText.text = "0000000000";
        this.player.resetScore();

        let transform = this.player.getComponent<Transform2D>("transform2d");

        transform.angle = -90;
        transform.position.x = 0;
        transform.position.y = 0;
        transform.centerOn(sceneTransform.position, sceneTransform.width, sceneTransform.height);

        this.foregroundTimer.transform.width = 156;

        this.onMatchStart(false);
    }

    private onPlayerCircleDrawn(sender: Kart, args: any): void {
        let noScore = "0000000000";
        let score = args.score.toString();
        let leftZeroes = noScore.slice(0, noScore.length - score.length);

        this.scoreValueText.text = (leftZeroes + score);
    }

    private onMatchStart(isEnter: boolean): void {
        this.getCountdownTweenConfig(this.countdownOne.transform, 0, false, false);
        this.getCountdownTweenConfig(this.countdownTwo.transform, 0.7, false, false);
        this.getCountdownTweenConfig(this.countdownThree.transform, 1.4, false, false);
        this.getCountdownTweenConfig(this.countdownFour.transform, 2.1, isEnter, true);
    }

    public onMatchTimeOut(sender: MatchTime, args: any): void {

        jcrGame.sceneActivator.pushScene("EndMatch", {
            score: this.player.score
        });

    }
}