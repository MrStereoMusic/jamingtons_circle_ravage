import { AnhaCoreEvent } from '../lib/AnhaCore/Events/AnhaCoreEvent';
import { Axis } from '../lib/AnhaCore/Physics/Collisions/Axis.enum';
import { Collisions2D } from '../lib/AnhaCore/Component/Physics/Collisions2D';
import { DrawTrackComponent } from './World/DrawTrackComponent';
import { Entity } from '../lib/AnhaCore/World/Entity';
import { jcrGame } from './Main/JCR';
import { KartInputController } from './KartInputController';
import { Origin } from '../lib/AnhaCore/Component/UI/Origin.enum';
import { ParticleCollection } from '../lib/AnhaCore/Component/Particles/ParticleCollection';
import { Physics2D } from '../lib/AnhaCore/Component/Physics/Physics2D';
import { RGBA } from '../lib/AnhaCore/UI/RGBA';
import { Side } from '../lib/AnhaCore/Physics/Collisions/Side.enum';
import { SlipTrackParticle } from './SlipTrackParticle';
import { Sprite } from '../lib/AnhaCore/Component/UI/Sprite';
import { Transform2D } from '../lib/AnhaCore/Component/UI/Transform2D';
import { Vector2 } from '../lib/AnhaCore/Physics/Vector2';
import { World } from '../lib/AnhaCore/World/World';

// TODO: different file
export enum COLLISION_LAYERS {
    BACKGROUND = 0,
    FOREGROUND = 1
}

export class Kart extends Entity {

    public score: number;
    public onCircleDrawn: AnhaCoreEvent<any>; // TODO: DrawnArgs

    constructor(world: World) {
        super();

        let stage = jcrGame.client.stage;

        this.score = 0;

        this.addComponent(new Collisions2D({
            category: COLLISION_LAYERS.FOREGROUND,
            group: COLLISION_LAYERS.BACKGROUND,
            bounds: {
                min: {
                    x: 0,
                    y: 0
                },
                max: {
                    x: 40,
                    y: 25
                }
            },
            keepInVirtualViewport: true,
            solid: true,
            onCollisionEnter: (collider: Entity, side: Side, axis: Axis) => {
                //
            },
            onVirtualViewportClamped: (side: Side) => {
                //
            }
        }));

        this.addComponent(new Physics2D({
            maxVelocity: {
                x: 350,
                y: 350
            },
            maxAcceleration: {
                x: 75,
                y: 75
            },
            acceleration: {
                x: 0,
                y: 0
            },
            mass: 10,
            friction: {
                x: 0.08,
                y: 0.08
            },
            floating: true,
            bounce: 0
        }));

        let transform = <Transform2D>this.addComponent(new Transform2D({
            width: 40,
            height: 25,
            position: {
                x: 0,
                y: 0
            },
            angle: -90,
            zIndex: 2
        }));

        transform.centerOn(new Vector2(0, 0), stage.width, stage.height);

        this.addComponent(new Sprite({
            image: jcrGame.imageLoader.getImage("sprites"),
            frame: {
                x: 0,
                y: 0
            },
            stage: stage
        }));

        let particleCollection = new ParticleCollection({
            parent: <Transform2D>transform,
            emitter: {
                enabled: false,
                particleCtor: SlipTrackParticle,
                particleConfig: {
                    life: 1.5,
                    view: new Sprite({
                        image: jcrGame.imageLoader.getImage("sprites"),
                        frame: {
                            x: 41,
                            y: 27
                        },
                        stage: stage
                    }),
                    physics: {
                        maxVelocity: {
                            x: 0,
                            y: 0
                        },
                        maxAcceleration: {
                            x: 0,
                            y: 0
                        },
                        mass: 50,
                        friction: {
                            x: 0,
                            y: 0
                        },
                        bounce: 0,
                        floating: true,
                        collidable: false
                    },
                    transform: {
                        width: 17,
                        height: 24,
                        zIndex: 1
                    }
                },
                maxParticles: 100,
                spawnRate: 0.05,
                bounds: {
                    min: {
                        x: 0,
                        y: 0
                    },
                    max: {
                        x: 0,
                        y: 50
                    }
                }
            }
        });

        this.addComponent(particleCollection);

        let particles = particleCollection.emitter.particles.all();
        let particle;
        let i;

        for (i = 0; i < particles.length; ++i) {
            particle = particles[i];

            world.addEntity(particle);
        }

        let drawTrackComponent = this.addComponent(new DrawTrackComponent({
            color: RGBA.LIGHTBLUE,
            thickness: 3,
            delay: 12
        }));

        this.onCircleDrawn = new AnhaCoreEvent<any>();
    }

    public resetScore(): void {
        this.score = 0;
    }

    public addScore(points: number): void {
        this.score += ~~points;
        
        this.onCircleDrawn.dispatch(this, {
            score: this.score
        });
    }

    public ignoreInput(): void {
        super.ignoreInput();

        let controller = this.getComponent<KartInputController>("kartInputController");

        controller.disable();
    }

    public listenToInput(): void {
        super.listenToInput();
        
        let controller = this.getComponent<KartInputController>("kartInputController");

        controller.enable();
    }
}