import { Particle } from '../lib/AnhaCore/Particles/Particle';
import { ParticleConfig } from '../lib/AnhaCore/Config/ParticleConfig';
import { Physics2D } from '../lib/AnhaCore/Component/Physics/Physics2D';
import { Transform2D } from '../lib/AnhaCore/Component/UI/Transform2D';

export class SlipTrackParticle extends Particle {

    constructor(config: ParticleConfig) {
        super({
            life: config.life,
            view: config.view,
            physics: config.physics,
            transform: config.transform
        });
    }

    public updateBehavior(d: number): void {
         let transform = this.getComponent<Transform2D>("transform2d");

         transform.effects.opacity = (this._life / this.defaultLife);
    }
}