const webpack = require('webpack')
const path = require('path')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
  entry: './src/Main/Main.ts',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'game.js'
  },
  resolve: {
    extensions: ['.js', '.ts']
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  plugins: [
    new UglifyJSPlugin()
  ]
};