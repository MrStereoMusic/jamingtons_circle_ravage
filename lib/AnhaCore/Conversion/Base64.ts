export class Base64 {

    public encode(value: string) {
        let val = value;

        try {
            val = btoa(val);
        } catch (e) {
            console.warn('Base64: could not encode string, with the following exception: ');
            console.warn(e);
        }

        return val;
    }

    public decode(enc: string) {
        let value = enc;

        try {
            value = atob(value);
        } catch (e) {
            console.warn('Base64: could not decode string, with the following exception: ');
            console.warn(e);
        }

        return value;
    }

}