import { AnhaCoreEvent } from '../Events/AnhaCoreEvent';
import { BoundingBox } from '../Physics/BoundingBox';
import { List } from '../Collection/List';
import { MathTool } from '../Tool/MathTool';
import { Particle } from './Particle';
import { ParticleEmitterConfig } from '../Config/ParticleEmitterConfig';
import { ParticleEmitterEventArgs } from './ParticleEmitterEventArgs';
import { Timer } from '../Timing/Timer';
import { Transform2D } from '../Component/UI/Transform2D';

export class ParticleEmitter {

    private parent: Transform2D;

    public readonly particles: List<Particle>;
    private maxParticles: number;

    private bounds: BoundingBox;

    private timer: Timer;
    private spawnRate: number;

    public readonly onSpawn: AnhaCoreEvent<ParticleEmitterEventArgs>;

    constructor(parent: Transform2D, config: ParticleEmitterConfig) {

        this.parent = parent;

        this.particles = new List<Particle>();
        this.maxParticles = config.maxParticles;
        this.spawnRate = config.spawnRate;
        this.bounds = new BoundingBox(config.bounds);

        this.onSpawn = new AnhaCoreEvent<ParticleEmitterEventArgs>();

        this.timer = new Timer({
            action: () => {
                this.emit();
            },
            interval: config.spawnRate
        });

        this.initialize(config.particleCtor, config.particleConfig);

        if (config.enabled) {
            this.enable();
        }
    }

    get spawnBox(): BoundingBox {
        return this.bounds;
    }

    private initialize(ctor: any, conf: any): void {
        let particle;
        let i;

        for (i = 0; i < this.maxParticles; ++i) {
            particle = new ctor(conf);

            this.particles.add(particle);

            this.onSpawn.dispatch(this, {
                particle
            });
        }
    }

    public changeSpawnRate(value: number): void {
        this.spawnRate = value;
    }

    public emit(): void {
        let lastParticle = this.particles.getLast();

        if (!lastParticle.isAlive()) {
            let transform = lastParticle.getComponent<Transform2D>("transform2d");
            let bounds = this.bounds;

            transform.position.x = bounds.min.x + MathTool.rand(0, bounds.max.x);
            transform.position.y = bounds.min.y;

            transform.angle = this.parent.angle;

            lastParticle.resetLife();

            this.particles.swap(this.particles.lastIndex(), 0);

            this.timer.reset();
        }
    }

    public receive(i: number): void {
        let deadParticle = this.particles.get(i);

        let transform = deadParticle.getComponent<Transform2D>("transform2d");

        // TODO: seriously, hide it.
        transform.position.x = -999999;
        transform.position.y = -999999;

        this.particles.swap(this.particles.lastIndex(), i);
    }

    public enable(): void {
        this.timer.unpause();
    }

    public disable(): void {
        this.timer.pause();
    }

    public update(d: number): void {

        this.timer.update(d);

        let i;
        let particles = this.particles.all();
        let len = this.maxParticles;

        for (i = 0; i < len; ++i) {

            if (!particles[i].update(d)) {
                this.receive(i);
            }

        }
    }
}