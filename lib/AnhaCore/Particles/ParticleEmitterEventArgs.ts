import { Particle } from './Particle';

export type ParticleEmitterEventArgs = {
    particle: Particle;
};