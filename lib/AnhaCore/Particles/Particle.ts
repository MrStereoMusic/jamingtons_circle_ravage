import { Entity } from '../World/Entity';
import { ParticleConfig } from '../Config/ParticleConfig';
import { Physics2D } from '../Component/Physics/Physics2D';
import { Transform2D } from '../Component/UI/Transform2D';

export abstract class Particle extends Entity {

    protected _life: number;
    protected defaultLife: number;

    constructor(config: ParticleConfig) {
        super();

        this._life = 0;
        this.defaultLife = config.life;

        this.addComponent(config.view);
        this.addComponent(new Transform2D(config.transform));
        this.addComponent(new Physics2D(config.physics));
    }

    get life(): number {
        return this._life;
    }

    public resetLife(): void {
        this._life = this.defaultLife;
    }

    public isAlive(): boolean {
        return (this._life > 0);
    }

    public update(d: number): boolean {

        this._life -= d;

        if (this.isAlive()) {
            this.updateBehavior(d);
            return true;
        }

        return false;
    }

    abstract updateBehavior(d: number): void;
}