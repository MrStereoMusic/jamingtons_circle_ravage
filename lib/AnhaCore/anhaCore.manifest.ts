export const AnhaCoreManifest = {
    developer: "Yvo Geldhof",
    version: {
        addition: "alpha",
        major: "0",
        minor: "9",
        build: "2"
    },
    year: "2017"
};