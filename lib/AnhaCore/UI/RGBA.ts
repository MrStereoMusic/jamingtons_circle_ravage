export class RGBA {
    static WHITE = [255, 255, 255, 255, 1];
    static GRAY = [33, 33, 33, 1];
    static BLACK = [0, 0, 0, 1];
    static LIGHTGRAY = [220, 220, 220, 1];
    static CORNFLOWERBLUE = [70, 170, 200, 1];
    static BLUE = [30, 100, 150, 1];
    static YELLOW = [235, 225, 50, 1];
    static RED = [222, 40, 40, 1];
    static GREEN = [60, 150, 35, 1];
    static BROWN = [150, 110, 90, 1];
    static VIOLET = [150, 60, 160, 1];
    static TRANSPARENT = [0, 0, 0, 0];

    static LIGHTBLUE = [187, 235, 255, 1];
}