import { PaddingConfig } from '../Config/PaddingConfig';

export class Padding {

    private _top: number;
    private _right: number;
    private _bottom: number;
    private _left: number;

    constructor(config: PaddingConfig) {
        this._top = config.top;
        this._right = config.right;
        this._bottom = config.bottom;
        this._left = config.left;
    }

    get top(): number {
        return this._top;
    }
    get right(): number {
        return this._right;
    }

    get bottom(): number {
        return this._bottom;
    }

    get left(): number {
        return this._left;
    }
}