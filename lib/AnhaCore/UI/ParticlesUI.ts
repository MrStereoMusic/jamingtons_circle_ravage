// import { Canvas } from '../Rendering/Shared/Canvas';
// import { Particle } from '../Particles/Particle';
// import { ParticleEmitter } from '../Particles/ParticleEmitter';
// import { ParticleSourceConfig } from '../Config/ParticleSourceConfig';
// import { SceneObject } from '../Scene/SceneObject';
// import { Transform2D } from '../Component/UI/Transform2D';
// import { View2D } from '../Component/UI/View2D';

// export class ParticlesUI extends SceneObject {

//     private emitter: ParticleEmitter;

//     constructor(config: ParticleSourceConfig) {
//         super(config.transform);

//         this.emitter = new ParticleEmitter(null, config.emitter);
//     }

//     public update(d: number): void {
//         let emitter = this.emitter;
//         let transform = this.transform;

//         emitter.spawnBox.sync(transform.position.x, transform.position.y, transform.width, transform.height);
//         emitter.update(d);
//     }

//     public render(stage: Canvas): void {
//         let emitter = this.emitter;
//         let particles = emitter.particles.all();
//         let len = particles.length;
//         let particle: Particle;
//         let view;
//         let transform;
//         let i;

//         for (i = 0; i < len; ++i) {
//             particle = particles[i];

//             view = particle.getComponent<View2D>("view2d");
//             transform = particle.getComponent<Transform2D>("transform2d");

//             view.renderer.render(view, transform);
//         }
//     }
// }