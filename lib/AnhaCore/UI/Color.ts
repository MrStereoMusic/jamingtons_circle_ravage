export class Color {

    private _r: number;
    private _g: number;
    private _b: number;
    private _a: number;

    private webGLValues: Array<number>;
    private maxColorValue: number;

    constructor(values: Array<number>) {

        if (values.length < 4) {
            console.error('Color: some values are missing.');
        }

        this._r = values[0];
        this._g = values[1];
        this._b = values[2];
        this._a = values[3];

        this.maxColorValue = 1 / 255;
        this.webGLValues = [];
    }

    get r(): number {
        return this._r;
    }

    get g(): number {
        return this._g;
    }

    get b(): number {
        return this._b;
    }

    get a(): number {
        return this._a;
    }

    public change(r: number, g: number, b: number, a: number): void {
        this._r = r;
        this._g = g;
        this._b = b;
        this._a = a;
    }

    public toWebGLValues(): Array<number> {
        let max = this.maxColorValue;

        this.webGLValues[0] = this._r * max;
        this.webGLValues[1] = this._g * max;
        this.webGLValues[2] = this._b * max;
        this.webGLValues[3] = this._a;
        
        return this.webGLValues;
    }
}