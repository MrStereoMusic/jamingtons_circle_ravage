// import { SceneObject } from '../Scene/SceneObject';
// import { Text } from '../Component/UI/Text';
// import { TextBoxConfig } from '../Config/TextBoxConfig';
// import { Transform2D } from '../Component/UI/Transform2D';

// export class TextBox extends SceneObject {

//     public readonly text: Text;

//     constructor(config: TextBoxConfig) {
//         super(config.transform);

//         this.text = new Text(config.text);
//     }

//     public update(d: number): void {
//         //
//     }

//     public render(ctx: CanvasRenderingContext2D, transform?: Transform2D): void {
//         let renderer = this.text.renderer;

//         this.transform = transform || this.transform;
//         renderer.render(ctx, this.text, this.transform);
//     }
// }