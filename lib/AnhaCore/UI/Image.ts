import { Canvas } from '../Rendering/Shared/Canvas';
import { ImageConfig } from '../Config/ImageConfig';
import { SceneObject } from '../Scene/SceneObject';
import { Sprite } from '../Component/UI/Sprite';
import { Transform2D } from '../Component/UI/Transform2D';

export class Image extends SceneObject {

    public readonly sprite: Sprite;

    constructor(config: ImageConfig) {
        super(config.transform);

        this.sprite = new Sprite(config.sprite);
        this.setRenderer(this.sprite.renderer);
    }

    public update(d: number): void {
        //
    }

    public render(stage: Canvas, transform?: Transform2D): void {
        this.transform = transform || this.transform;
        this.sprite.renderer.render(this.sprite, this.transform);
    }
}