import { AnhaCoreEvent } from '../Events/AnhaCoreEvent';
import { Canvas } from '../Rendering/Shared/Canvas';
import { ClientConfig } from '../Config/ClientConfig';
import { Vector2 } from '../Physics/Vector2';

export class Client {

    private _stage: Canvas;
    private _resolution: Vector2;
    private _ratio: number;
    public smoothRendering: boolean;

    public readonly onResize: AnhaCoreEvent<UIEvent>;

    constructor(config: ClientConfig) {

        this._resolution = new Vector2(config.resolutionX, config.resolutionY);

        this.createStage(config.stageId, config.enableWebGL);

        this.smoothRendering = true;

        this.onResize = new AnhaCoreEvent<UIEvent>();

        window.addEventListener("resize", (args: UIEvent) => {
            this.resize();
            this.onResize.dispatch(this, args);
        }, false);
    }

    get resolution(): Vector2 {
        return this._resolution;
    }

    get stage(): Canvas {
        return this._stage;
    }

    get ratio(): number {
        return this._ratio;
    }

    public disableImageSmoothing() {

        if (this.smoothRendering) {

            document.head.innerHTML += `
                <style type="text/css">
                    canvas {
                        image-rendering: optimizeSpeed;
                        image-rendering: -moz-crisp-edges;
                        image-rendering: -o-crisp-edges;
                        image-rendering: -webkit-optimize-contrast;
                        image-rendering: optimize-contrast;
                        image-rendering: crisp-edges;
                        image-rendering: pixelated;
                        -ms-interpolation-mode: nearest-neighbor;
                    }
                </style>
            `;

            this.smoothRendering = false;
        }

    }

    private resize() {
        let stage = this._stage;
        let elementStyle = stage.domElement.style;
        let windowWidth = window.innerWidth;
        let windowHeight = window.innerHeight;
        let ratio = Math.min((windowWidth / this.resolution.x), (windowHeight / this.resolution.y));

        this._ratio = ratio;

        elementStyle.position = "absolute";
        elementStyle.transformOrigin = "0 0";
        elementStyle.transform = "scale(" + ratio + ")";
        elementStyle.left = (windowWidth * .5) - (this.resolution.x * ratio * .5) + "px";
        elementStyle.top = (windowHeight * .5) - (this.resolution.y * ratio * .5) + "px";
    }

    private createStage(name: string, enableWebGL: boolean): void {

        this._stage = new Canvas({
            name: name,
            width: this.resolution.x,
            height: this.resolution.y,
            enableWebGL: enableWebGL
        });

        this.resize();

        document.body.appendChild(this._stage.domElement);
    }

    public enterFullscreen(): void {
        this.stage.domElement.requestFullscreen();
    }

    public exitFullscreen(): void {
        document.webkitCancelFullScreen();
        document.exitFullscreen();
    }
}