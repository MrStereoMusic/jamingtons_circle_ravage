import { AnhaCoreGameConfig } from '../Config/AnhaCoreGameConfig';

export class GameConfig implements AnhaCoreGameConfig {

    public readonly gameName: string;
    public readonly resolutionX: number;
    public readonly resolutionY: number;
    public readonly initSceneCode: string;
    public readonly developerMode: boolean;
    public readonly imageSmoothing: boolean;
    public readonly images: { [key: string]: string };
    public readonly enableWebGL: boolean;
    public readonly developer: string;
    public readonly copyright: string;
    public readonly version: string;

    constructor(config: AnhaCoreGameConfig) {
        Object.assign(this, config);
    }
}