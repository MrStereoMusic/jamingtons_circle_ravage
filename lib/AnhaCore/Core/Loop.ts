import { Action } from './Action';
import { AnhaCoreGame } from './AnhaCoreGame';
import { RunMode } from './RunMode.enum';

export class Loop {

    private game: AnhaCoreGame;

    private last: number;
    private deltaTime: number;

    private defaultStep: number;
    private step: number;

    private runMode: RunMode;

    private callback: Action;
    private running: boolean;

    constructor(game: AnhaCoreGame) {
        this.game = game;
        this.last = 0;
        this.running = false;
        this.callback = null;
        this.deltaTime = 0;

        this.defaultStep = 16;
        this.step = this.defaultStep;
    }

    public start(): void {

        if (!this.running) {
            this.running = true;

            this.callback = (now: number): void => {
                this.onNewFrame(now);
            };

            this.requestFrame();
        }

    }

    public stop(): void {
        this.running = false;
    }

    public applyRunMode(mode: RunMode): void {

        if (mode == RunMode.FIXED_TIMESTEP) {
            this.step = this.defaultStep;
        } else {
            this.step = Infinity;
        }

        this.runMode = mode;
    }

    private requestFrame(): void {
        requestAnimationFrame(this.callback);
    }

    private onNewFrame(now: number): void {
        let elapsedTime = (now - this.last);

        this.deltaTime = Math.min(elapsedTime, this.step) * 0.001;

        this.game.processNewFrame(this.deltaTime);

        this.last = now;
        this.running && this.requestFrame();
    }

}