export class Inputable {

    protected acceptInput: boolean;

    public ignoreInput(): void {
        this.acceptInput = false;
    }

    public listenToInput(): void {
        this.acceptInput = true;
    }
}