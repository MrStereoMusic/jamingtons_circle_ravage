export interface Updatable {
    update(d: number, ...args: any[]): void;
}