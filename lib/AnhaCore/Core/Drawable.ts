import { Canvas } from '../Rendering/Shared/Canvas';

export interface Drawable {
    render(stage: Canvas, ...args: any[]): void;
}