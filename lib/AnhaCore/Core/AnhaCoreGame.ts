import { AnhaCoreEvent } from '../Events/AnhaCoreEvent';
import { AnhaCoreGameConfig } from '../Config/AnhaCoreGameConfig';
import { AnhaCoreManifest } from '../anhaCore.manifest';
import { Canvas } from '../Rendering/Shared/Canvas';
import { Client } from './Client';
import { EventArgs } from '../Events/EventArgs';
import { GameConfig } from './GameConfig';
import { ImageLoader } from '../Loader/ImageLoader';
import { InputDeviceLocator } from '../Input/InputDeviceLocator';
import { LocalData } from '../LocalData/LocalData';
import { Loop } from './Loop';
import { RunMode } from './RunMode.enum';
import { Scene } from '../Scene/Scene';
import { SceneActivator } from '../Scene/SceneActivator';

export class AnhaCoreGame {

    private stage: Canvas;
    public readonly client: Client;
    public readonly imageLoader: ImageLoader;
    public readonly sceneActivator: SceneActivator;
    public readonly data: LocalData;
    public readonly input: InputDeviceLocator;

    private loop: Loop;

    private developerMode: boolean;
    public totalFrames: number;

    private config: GameConfig;

    public readonly onPostUpdate: AnhaCoreEvent<EventArgs>;
    public readonly onPreUpdate: AnhaCoreEvent<EventArgs>;

    constructor(config: AnhaCoreGameConfig, scenes: Array<Scene>) {
        let gameName: string = (config.gameName).replace(/ /g, '');

        this.client = new Client({
            stageId: gameName,
            resolutionX: config.resolutionX,
            resolutionY: config.resolutionY,
            enableWebGL: config.enableWebGL
        });

        this.config = new GameConfig(config);
        this.loop = new Loop(this);
        this.imageLoader = new ImageLoader();
        this.sceneActivator = new SceneActivator(this.client, scenes);
        this.developerMode = this.config.developerMode;

        this.stage = this.client.stage;

        if (!config.imageSmoothing) {
            this.client.disableImageSmoothing();
        }

        this.data = new LocalData(gameName);

        this.onPreUpdate = new AnhaCoreEvent<EventArgs>();
        this.onPostUpdate = new AnhaCoreEvent<EventArgs>();

        this.input = new InputDeviceLocator(this);

        this.notifyAboutRenderer();
        this.run();
    }

    private notifyAboutRenderer(): void {

        if (this.developerModeEnabled()) {

            if (!this.stage.isWebGL) {
                console.warn("AnhaCoreGame: using Canvas2D renderers.");
            } else {
                console.warn("AnhaCoreGame: using WebGL renderers.");
            }
        }

    }

    public run(): void {
        this.preloadAssets();
    }

    private preloadAssets(): void {
        this.imageLoader.setCallback(() => {
            this.onImagesPreloaded();
        });

        this.imageLoader.loadImages(this.config.images);
    }

    private onImagesPreloaded(): void {
        let version = AnhaCoreManifest.version;

        if (this.developerMode) {
            console.log("Running AnhaCore Engine " + version.addition + " v" + version.major + "." + version.minor + "." + version.build);
        }

        this.sceneActivator.pushScene(this.config.initSceneCode, {
            score: 300
        });
        this.loop.start();
    }

    public setRunMode(mode: RunMode) {
        this.loop.applyRunMode(mode);
    }

    public developerModeEnabled(): boolean {
        return this.developerMode;
    }

    public processNewFrame(d: number): void {
        this.onPreUpdate.dispatch(this, null);
        this.sceneActivator.update(d);
        this.onPostUpdate.dispatch(this, null);

        this.sceneActivator.render(this.stage);
    }
}