import { BoundingBox } from '../Physics/BoundingBox';
import { Camera2DConfig } from '../Config/Camera2DConfig';
import { MathTool } from '../Tool/MathTool';
import { Matrix2D } from '../Rendering/Shared/Matrix2D';
import { Transform2D } from '../Component/UI/Transform2D';
import { Transitions } from '../Animation/Tweening/Transition/Transitions';
import { Vector2 } from '../Physics/Vector2';

export class Camera2D {

    private target: Transform2D;

    private viewport: Vector2;
    private worldSize: BoundingBox;

    private transform: Transform2D;
    private smoothness: number;

    constructor(config: Camera2DConfig) {

        this.viewport = new Vector2(config.viewport.width, config.viewport.height);
        this.worldSize = new BoundingBox(config.bounds);

        this.smoothness = Number(Math.round(config.smoothness).toFixed(1));

        this.transform = new Transform2D({
            width: this.worldSize.magnitudeX(),
            height: this.worldSize.magnitudeY(),
            origin: {
                x: config.focus.x,
                y: config.focus.y
            }
        });
    }

    public setWorldSize(x: number, y: number): void {
        this.worldSize.max.x = x;
        this.worldSize.max.y = y;
    }

    public transformation(): Transform2D {
        return this.transform;
    }

    public follow(target: Transform2D) {
        this.target = target;
    }

    public update(d: number): void {

        if (this.target) {

            let transform = this.transform;
            let target = this.target;
            let worldSize = this.worldSize;
            let interpolation = d * this.smoothness;

            let toX = ((target.position.x + target.centerX()) - transform.origin.x);
            let toY = ((target.position.y + target.centerY()) - transform.origin.y);

            this.transform.translation.x = -Transitions.ease(d, -transform.translation.x,
                toX - -transform.translation.x, interpolation);
            this.transform.translation.y = -Transitions.ease(d, -transform.translation.y,
                toY - -transform.translation.y, interpolation);

            this.transform.translation.x = -MathTool.clamp(-this.transform.translation.x, worldSize.min.x, worldSize.max.x - this.viewport.x);
            this.transform.translation.y = -MathTool.clamp(-this.transform.translation.y, worldSize.min.y, worldSize.max.y - this.viewport.y);

            this.transform.position.x = 1;
            this.transform.position.y = 1;
        }
    }
}