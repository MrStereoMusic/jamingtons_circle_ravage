export enum RunMode {
    VARIABLE_TIMESTEP,
    FIXED_TIMESTEP
}