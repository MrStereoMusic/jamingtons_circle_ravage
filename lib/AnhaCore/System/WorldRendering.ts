import { Canvas } from '../Rendering/Shared/Canvas';
import { Entity } from '../World/Entity';
import { Renderer } from '../Rendering/Shared/Renderer';
import { System } from './System';
import { Transform2D } from '../Component/UI/Transform2D';
import { Vector2 } from '../Physics/Vector2';
import { World } from '../World/World';
import { WorldRendererC2D } from '../Rendering/Canvas2D/WorldRendererC2D';

export class WorldRendering extends System {

    private viewport: Vector2;
    private cameraTransform: Transform2D;

    public renderables: Array<Entity>;
    public readonly renderer: Renderer;

    constructor(stage: Canvas) {
        super({
            tag: "rendering",
            requiredComponents: ["transform2d", "view2d"]
        });

        this.viewport = new Vector2(stage.width, stage.height);

        if (stage.isWebGL) {
            //this.renderer = new WorldRendererGL2D(stage);
        } else {
            this.renderer = new WorldRendererC2D(stage);
        }
    }

    public initialize(world: World): void {
        this.cameraTransform = world.camera.transformation();
    }

    public preUpdateSystem(d: number, world: World): void {
        //
    }

    public updateSystem(d: number, world: World): void {
        let cameraPosition = this.cameraTransform.translation;
        let camPosX = Math.abs(cameraPosition.x);
        let camPosY = Math.abs(cameraPosition.y);

        let viewport = this.viewport;
        let transform;
        let position;

        this.renderables = this.entities.where(entity => {
            transform = entity.getComponent<Transform2D>("transform2d");
            position = transform.position;

            return (
                position.x + transform.width >= camPosX &&
                position.x < camPosX + viewport.x &&
                position.y + transform.height >= camPosY &&
                position.y < camPosY + viewport.y
            );
        });

        let firstTransform;
        let secondTransform;

        this.renderables.sort((first: Entity, second: Entity) => {
            firstTransform = first.getComponent<Transform2D>("transform2d");
            secondTransform = second.getComponent<Transform2D>("transform2d");

            return firstTransform.zIndex - secondTransform.zIndex;
        });
    }

    public postUpdateSystem(d: number, world: World): void {
        //
    }
}