import { AnhaCoreEvent } from '../Events/AnhaCoreEvent';
import { Axis } from '../Physics/Collisions/Axis.enum';
import { Collisions } from './Collisions';
import { Entity } from '../World/Entity';
import { MathTool } from '../Tool/MathTool';
import { Physics2D } from '../Component/Physics/Physics2D';
import { PhysicsSystemEventArgs } from './PhysicsSystemEventArgs';
import { System } from './System';
import { Transform2D } from '../Component/UI/Transform2D';
import { World } from '../World/World';

export class Physics extends System {

    private physicsEventArgs: PhysicsSystemEventArgs;

    public readonly onProcessingStart: AnhaCoreEvent<PhysicsSystemEventArgs>;
    public readonly onCollisionsRequest: AnhaCoreEvent<PhysicsSystemEventArgs>;

    constructor() {
        super({
            tag: "physics",
            requiredComponents: ["physics2d", "transform2d"]
        });

        this.onProcessingStart = new AnhaCoreEvent<PhysicsSystemEventArgs>();
        this.onCollisionsRequest = new AnhaCoreEvent<PhysicsSystemEventArgs>();

        this.physicsEventArgs = {
            entity: null,
            axis: null,
            deltaTime: 0
        };
    }

    public initialize(world: World): void {
        //       
    }

    public preUpdateSystem(d: number, world: World): void {
        //
    }

    public updateSystem(d: number, world: World): void {
        let entities = this.entities.all();
        let physics: Physics2D;
        let transform: Transform2D;
        let i;
        let len = entities.length;
        let entity: Entity;
        let evtArgs = this.physicsEventArgs;

        evtArgs.deltaTime = d;

        for (i = 0; i < len; ++i) {
            entity = entities[i];

            physics = entity.getComponent<Physics2D>("physics2d");
            transform = entity.getComponent<Transform2D>("transform2d");

            if (physics.collidable) {
                evtArgs.entity = entity;
                this.onProcessingStart.dispatch(this, evtArgs);
            }

            physics.velocity.x += physics.acceleration.x;

            if (physics.friction.x > 0) {
                physics.velocity.x *= Math.pow(physics.friction.x, d);
            }

            physics.velocity.x = MathTool.clamp(physics.velocity.x, -physics.maxVelocity.x, physics.maxVelocity.x);
            transform.position.x += (physics.velocity.x * Math.cos(Math.PI / 180 * transform.angle)) * d;

            if (physics.collidable) {
                evtArgs.axis = Axis.X;
                this.onCollisionsRequest.dispatch(this, evtArgs);
            }

            physics.velocity.y += physics.acceleration.y;

            if (!physics.floating) {
                physics.velocity.y += (physics.mass + world.gravity) * d;
            }

            if (physics.friction.y > 0) {
                physics.velocity.y *= Math.pow(physics.friction.y, d);
            }

            physics.velocity.y = MathTool.clamp(physics.velocity.y, -physics.maxVelocity.y, physics.maxVelocity.y);
            transform.position.y += (physics.velocity.y * Math.sin(Math.PI / 180 * transform.angle)) * d;

            if (physics.collidable) {
                evtArgs.axis = Axis.Y;
                this.onCollisionsRequest.dispatch(this, evtArgs);
            }

            physics.acceleration.x = 0;
            physics.acceleration.y = 0;
        }
    }

    public postUpdateSystem(d: number, world: World): void {
        //
    }
}