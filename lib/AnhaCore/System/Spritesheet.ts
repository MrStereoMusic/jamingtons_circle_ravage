import { Sprite } from '../Component/UI/Sprite';
import { SpriteAnimation } from '../Component/Animation/Sprite/SpriteAnimation';
import { SpriteAnimations } from '../Component/Animation/Sprite/SpriteAnimations';
import { System } from './System';
import { View2D } from '../Component/UI/View2D';
import { World } from '../World/World';

export class Spritesheet extends System {

    constructor() {
        super({
            tag: "spritesheet",
            requiredComponents: ["spriteAnimations", "view2d"]
        });
    }

    public initialize(world: World): void {
        //
    }

    public preUpdateSystem(d: number, world: World): void {
        let entities = this.entities.all();
        let anims;

        entities.forEach(entity => {
            anims = entity.getComponent<SpriteAnimations>("spriteAnimations");
            anims.setAnimation(anims);
        });
    }

    public updateSystem(d: number, world: World): void {
        let currentAnimation;
        let anims;
        let view;

        this.entities.all().forEach(entity => {
            anims = entity.getComponent<SpriteAnimations>("spriteAnimations");
            currentAnimation = anims.animationSet.get(anims.currentAnimKey);
            view = entity.getComponent<View2D>("view2d");

            if (anims.running) {
                anims.accumulator += d;

                if (anims.accumulator >= anims.tickSpeed) {
                    anims.accumulator = 0;

                    ++anims.currentColumn;

                    if (anims.currentColumn >= currentAnimation.columns) {
                        anims.currentColumn = 0;

                        ++anims.currentRow;

                        if (anims.currentRow >= currentAnimation.rows) {
                            anims.currentRow = 0;

                            if (anims.nextAnimKey) {
                                anims.currentAnimKey = anims.nextAnimKey;
                                anims.nextAnimKey = null;
                            } else {
                                anims.running = currentAnimation.loop;
                            }
                        }
                    }
                }

                this.syncFrame(view, currentAnimation, anims.currentColumn, anims.currentRow);
            }
        });
    }

    public postUpdateSystem(d: number, world: World): void {
        //
    }

    private syncFrame(view: Sprite, currentAnimation: SpriteAnimation, currentColumn: number, currentRow: number): void {
        let frameSize = currentAnimation.frameSize;
        let baseFrame = currentAnimation.baseFrame;

        view.frame.x = baseFrame.x + (currentColumn * frameSize.x);
        view.frame.y = baseFrame.y + (currentRow * frameSize.y);
    }
}
