import { Entity } from '../World/Entity';
import { List } from '../Collection/List';
import { SystemConfig } from '../Config/SystemConfig';
import { Updatable } from '../Core/Updatable';
import { World } from '../World/World';
import { WorldEventArgs } from '../World/WorldEventArgs';

export abstract class System {

    public readonly tag: string;

    protected entities: List<Entity>;
    private requiredComponents: Array<string>;

    constructor(config: SystemConfig) {
        this.tag = config.tag;
        this.requiredComponents = config.requiredComponents;
        this.entities = new List<Entity>();
    }

    public addSubscriptions(world: World): void {
        world.onEntityAdded.subscribe(this, this.onEntityAddedToWorld);
        world.onEntityRemoved.subscribe(this, this.onEntityRemovedFromWorld);
    }

    protected hasRequiredComponents(entity: Entity): boolean {
        let tag;

        for (tag of this.requiredComponents) {

            if (!entity.hasComponent(tag)) {
                return false;
            }
        }

        return true;
    }

    private onNewEntity(entity: Entity): void {

        if (!this.hasRequiredComponents(entity)) {
            return;
        }

        entity.onComponentAdded.subscribe(this, this.onEntityModified);
        entity.onComponentRemoved.subscribe(this, this.onEntityModified);

        this.entities.add(entity);
    }

    private onDeprecatedEntity(entity: Entity): void {

        if (!this.entities.contains(entity)) {
            return;
        }

        entity.onComponentAdded.unsubscribe(this);
        entity.onComponentRemoved.unsubscribe(this);

        this.entities.remove(entity);
    }

    private onEntityAddedToWorld(sender: Entity, args: WorldEventArgs): void {
        this.onNewEntity(args.entity);
    }

    private onEntityRemovedFromWorld(sender: Entity, args: WorldEventArgs): void {
        this.onDeprecatedEntity(args.entity);
    }

    private onEntityChildAdded(sender: Entity, args: WorldEventArgs): void {
        this.onNewEntity(args.entity);
    }

    private onEntityChildRemoved(sender: Entity, args: WorldEventArgs): void {
        this.onDeprecatedEntity(args.entity);
    }

    private onEntityModified(sender: Entity, args: WorldEventArgs): void {
        let entity = args.entity;

        if (!this.entities.contains(entity) || this.hasRequiredComponents(entity)) {
            return;
        }

        entity.onComponentAdded.unsubscribe(this);
        entity.onComponentRemoved.unsubscribe(this);

        this.entities.remove(entity);
    }

    public dispose(): void {
        this.entities = null;
    }

    public reset(): void {
        // override
    }

    abstract initialize(world: World): void;

    abstract preUpdateSystem(d: number, world: World): void;

    abstract updateSystem(d: number, world: World): void;

    abstract postUpdateSystem(d: number, world: World): void;
}