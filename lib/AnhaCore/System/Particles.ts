import { ParticleCollection } from '../Component/Particles/ParticleCollection';
import { System } from './System';
import { Transform2D } from '../Component/UI/Transform2D';
import { World } from '../World/World';

export class Particles extends System {

    constructor() {
        super({
            tag: "particles",
            requiredComponents: ["particleCollection", "transform2d"]
        });
    }

    public initialize(world: World): void {
        //
    }

    public preUpdateSystem(d: number, world: World): void {
        //
    }

    public updateSystem(d: number, world: World): void {
        let entities = this.entities.all();
        let emitter;
        let collection: ParticleCollection;
        let transform: Transform2D;

        entities.forEach(entity => {
            transform = entity.getComponent<Transform2D>("transform2d");
            collection = entity.getComponent<ParticleCollection>("particleCollection");
            emitter = collection.emitter;

            emitter.bounds.sync(transform.position.x, transform.position.y);
            emitter.update(d);
        });
    }

    public postUpdateSystem(d: number, world: World): void {
        //
    }
}