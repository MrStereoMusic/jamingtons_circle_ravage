import { Axis } from '../Physics/Collisions/Axis.enum';
import { BroadphaseTest } from '../Physics/Collisions/BroadphaseTest';
import { BroadphaseTestResult } from '../Physics/Collisions/BroadphaseTestResult';
import { Collisions2D } from '../Component/Physics/Collisions2D';
import { Entity } from '../World/Entity';
import { List } from '../Collection/List';
import { MathTool } from '../Tool/MathTool';
import { Physics } from './Physics';
import { Physics2D } from '../Component/Physics/Physics2D';
import { PhysicsSystemEventArgs } from './PhysicsSystemEventArgs';
import { Side } from '../Physics/Collisions/Side.enum';
import { System } from './System';
import { Transform2D } from '../Component/UI/Transform2D';
import { Vector2 } from '../Physics/Vector2';
import { World } from '../World/World';

export class Collisions extends System {

    private tester: BroadphaseTest;
    private testResult: BroadphaseTestResult;
    private lastCollided: List<Collisions2D>;
    private collidableEntities: Array<Entity>;
    private skipEntity: boolean;
    private virtualViewport: Vector2; // worldsize

    constructor() {
        super({
            tag: "collisions",
            requiredComponents: ["collisions2d", "transform2d"]
        });

        this.tester = new BroadphaseTest();
        this.testResult = new BroadphaseTestResult();
        this.lastCollided = new List<Collisions2D>();
        this.skipEntity = false;
        this.collidableEntities = [];
    }

    public initialize(world: World): void {
        let physicsSystem = world.systemByTag<Physics>("physics");

        if (!physicsSystem) {
            console.error(`Collisions: couldn't find physics system.`);

            return;
        }

        physicsSystem.onProcessingStart.subscribe(this, this.onProcessingStart);
        physicsSystem.onCollisionsRequest.subscribe(this, this.onCollisionRequest);

        this.virtualViewport = world.size;
    }

    public preUpdateSystem(d: number, world: World): void {
        this.lastCollided.clear();
        this.collidableEntities = [];
    }

    public onProcessingStart(sender: Physics, args: PhysicsSystemEventArgs): void {
        let entityA = args.entity;
        let collisionsA = entityA.getComponent<Collisions2D>("collisions2d");
        let transform = entityA.getComponent<Transform2D>("transform2d");
        let collisionsB;

        if (!entityA.hasComponent("collisions2d") || collisionsA.disableSelfCheck) {
            this.skipEntity = true;

            return;
        }

        this.collidableEntities = this.entities.where(entityB => {
            collisionsB = entityB.getComponent<Collisions2D>("collisions2d");

            return (
                entityA != entityB &&
                (collisionsA.group & collisionsB.category) != 0 &&
                (collisionsA.category & collisionsB.group) != 0
            );
        });
    }

    public onCollisionRequest(sender: Physics, args: PhysicsSystemEventArgs): void {

        if (this.skipEntity) {
            return;
        }

        let entities = this.entities;
        let tester = this.tester;
        let collidableEntities = this.collidableEntities;

        let entityA = args.entity;
        let collisionsA = entityA.getComponent<Collisions2D>("collisions2d");
        let transformA = entityA.getComponent<Transform2D>("transform2d");

        let collisionsB: Collisions2D;
        let transformB: Transform2D;

        let position = transformA.position;
        let info: BroadphaseTestResult;
        let axis = args.axis;

        collisionsA.previousBounds = Object.create(collisionsA.bounds);
        collisionsA.bounds.sync(transformA.position.x, transformA.position.y, transformA.width, transformA.height);

        collidableEntities.forEach(entityB => {
            collisionsB = entityB.getComponent<Collisions2D>("collisions2d");
            transformB = entityB.getComponent<Transform2D>("transform2d");

            if (!collisionsB.isCollidable(entityA, axis, collisionsA.previousBounds)) {
                return;
            }

            collisionsB.bounds.sync(transformB.position.x, transformB.position.y, transformB.width, transformB.height);

            if (tester.testAABB(entityA, entityB, collisionsA.bounds, collisionsB.bounds, args.axis)) {
                info = tester.result;

                if (!collisionsA.lastCollision || collisionsA.lastCollision.collider != info.withCollider) {
                    collisionsA.onCollisionEnter(entityB, info.side, args.axis);
                }

                if (!collisionsB.lastCollision || collisionsB.lastCollision.collider != info.collider) {
                    collisionsB.onCollisionEnter(entityA, info.oppositeSide, args.axis);
                }

                collisionsA.lastCollision = info;
                collisionsB.lastCollision = info;

                if (collisionsB.solid) {

                    if (entityA.hasComponent("physics2d")) {
                        let physicsA = entityA.getComponent<Physics2D>("physics2d");

                        this.separateEntity(args.deltaTime, info, physicsA, collisionsA, transformA, args.axis);
                    }

                } else {
                    collisionsA.onCollisionStay(entityB, info.side, args.axis);
                    collisionsB.onCollisionStay(entityA, info.oppositeSide, args.axis);
                }

                if (!this.lastCollided.contains(collisionsA)) {
                    this.lastCollided.add(collisionsA);
                }
            }
        });
    }

    public updateSystem(d: number, world: World): void {
        //
    }

    public postUpdateSystem(d: number, world: World): void {
        this.virtualViewportClampEntities();
        this.exitLastCollisions();
        this.skipEntity = false;
    }

    private virtualViewportClampEntities(): void {
        let entities = this.entities.all();
        let len = entities.length;
        let i;
        let viewport = this.virtualViewport;
        let collisionsA;
        let transformA;
        let entity: Entity;
        let bounds;
        let toPositionX;
        let toPositionY;
        let side;
        let physics;

        for (i = 0; i < entities.length; ++i) {
            entity = entities[i];
            collisionsA = entity.getComponent<Collisions2D>("collisions2d");

            if (collisionsA.keepInVirtualViewport) {
                transformA = entity.getComponent<Transform2D>("transform2d");

                bounds = collisionsA.bounds;

                toPositionX = MathTool.clamp(transformA.position.x, 0, viewport.x - transformA.width);
                toPositionY = MathTool.clamp(transformA.position.y, 0, viewport.y - transformA.height);

                if (entity.hasComponent("physics2d")) {
                    physics = entity.getComponent("physics2d");

                    if (toPositionX != transformA.position.x) {
                        side = <Side>(toPositionX > transformA.position.x ? 1 : 3);

                        physics.velocity.x = 0;
                        physics.acceleration.x = 0;
                    }

                    if (toPositionY != transformA.position.y) {
                        side = <Side>(toPositionY > transformA.position.y ? 0 : 2);

                        physics.velocity.y = 0;
                        physics.acceleration.y = 0;
                    }

                    if (side) {
                        collisionsA.onVirtualViewportClamped(side);
                    }
                }

                transformA.position.x = toPositionX;
                transformA.position.y = toPositionY;
            }
        }
    }

    private exitLastCollisions(): void {
        let lastCollided = this.lastCollided.where(collisions2D => collisions2D.solid && collisions2D.lastCollision);
        let lastInfo: BroadphaseTestResult;

        lastCollided.forEach(collisions2D => {
            lastInfo = collisions2D.lastCollision;

            collisions2D.onCollisionExit(lastInfo.collider, lastInfo.side, lastInfo.axis);

            collisions2D.lastCollision = null;
        });
    }

    private separateEntity(d: number, info: BroadphaseTestResult, physics: Physics2D, collisions: Collisions2D, transform: Transform2D, axis: Axis): void {
        let side = info.side;
        let penetration = info.penetration;
        let position = transform.position;

        if (axis == Axis.X) {

            if (side == Side.LEFT) {
                position.x += penetration;
            }

            if (side == Side.RIGHT) {
                position.x -= penetration;
            }

            physics.velocity.x *= physics.bounce;
            physics.acceleration.x = 0;

            if (physics.bounce != 0) {
                physics.velocity.x *= -1;
            }

            return;
        }

        if (side == Side.TOP) {
            position.y += penetration;
            physics.velocity.y *= physics.bounce;
            physics.acceleration.y = 0;
        }

        if (side == Side.BOTTOM) {
            position.y -= penetration;
            physics.velocity.y *= physics.bounce;
            physics.acceleration.y = 0;
        }

        if (physics.bounce != 0) {
            physics.velocity.y *= -1;
        }

        position = transform.position;

        collisions.bounds.sync(position.x, position.y, transform.width, transform.height);
    }
}