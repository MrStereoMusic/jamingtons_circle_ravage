import { System } from './System';
import { TweenAnimations } from '../Component/Animation/Tweening/TweenAnimations';
import { World } from '../World/World';

export class Tweening extends System {

    constructor() {
        super({
            tag: "tweening",
            requiredComponents: ["tweenAnimations"]
        });
    }

    public initialize(world: World): void {
        //
    }

    public preUpdateSystem(d: number, world: World): void {
        let entities = this.entities.all();
        let anims;

        entities.forEach(entity => {
            anims = entity.getComponent<TweenAnimations>("tweenAnimations");

            if (anims.setAnimation) {
                anims.setAnimation(anims);
            }
        });
    }

    public updateSystem(d: number, world: World): void {
        let tweenComponent;
        let tweens;
        let entities = this.entities.all();

        entities.forEach(entity => {
            tweenComponent = entity.getComponent<TweenAnimations>("tweenAnimations");
            tweens = tweenComponent.tweens;

            tweens.update(d);
        });
    }

    public postUpdateSystem(d: number, world: World): void {
        //
    }
}