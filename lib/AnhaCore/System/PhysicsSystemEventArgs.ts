import { Axis } from '../Physics/Collisions/Axis.enum';
import { Entity } from '../World/Entity';

export interface PhysicsSystemEventArgs {
    entity: Entity;
    axis?: Axis;
    deltaTime: number;
};