import { AnhaCoreEvent } from '../Events/AnhaCoreEvent';
import { Component } from '../Component/Component';
import { Inputable } from '../Core/Inputable';
import { WorldEventArgs } from './WorldEventArgs';

export class Entity extends Inputable {

    private id: number;
    private components: Map<string, Component>;

    public readonly onComponentAdded: AnhaCoreEvent<WorldEventArgs>;
    public readonly onComponentRemoved: AnhaCoreEvent<WorldEventArgs>;

    constructor() {
        super();
        
        this.id = null;
        this.components = new Map<string, Component>();
        
        this.onComponentAdded = new AnhaCoreEvent<WorldEventArgs>();
        this.onComponentRemoved = new AnhaCoreEvent<WorldEventArgs>();
    }

    get ID(): number {
        return this.id;
    }

    public attachID(id: number): boolean {

        if (this.id == null) {
            this.id = id;

            return true;
        }

        return false;
    }

    public hasComponent(tag: string): boolean {
        return this.components.has(tag);
    }

    public addComponent(component: Component): Component {

        if (this.hasComponent(component.tag)) {
            console.error(`Entity.addComponent: component#${component.tag} already exists.`);

            return;
        }

        this.components.set(component.tag, component);

        this.onComponentAdded.dispatch(this, {
            entity: this
        });

        return component;
    }

    public removeComponent(tag: string): boolean {

        if (!this.hasComponent(tag)) {
            console.error(`Entity.removeComponent: component#${tag} does not exist.`);

            return false;
        }

        this.components.delete(tag);

        this.onComponentRemoved.dispatch(this, {
            entity: this
        });

        return true;
    }

    public getComponent<T extends Component>(tag: string): T {

        if (!this.hasComponent(tag)) {
            console.error(`Entity.getComponent: component#${tag} does not exists.`);

            return null;
        }

        return <T>this.components.get(tag);
    }
}