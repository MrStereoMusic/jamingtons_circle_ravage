import { Component } from '../../Component/Component';

export interface ComponentConstructor {
    new (): Component;
}