import { Component } from '../../Component/Component';
import { ComponentConstructor } from './ComponentConstructor';
import { Entity } from '../Entity';
import { EntityConfig } from '../../Config/EntityConfig';

export class EntityFactory {

    private availableComponents: Map<string, ComponentConstructor>;

    constructor(components: Map<string, ComponentConstructor>) {
        this.availableComponents = components;
    }

    public createFrom(config: EntityConfig): Entity {
        let components = new Map<string, Component>();
        let entity = new Entity();
        let DynamicComponent;
        let componentConfig;
        let key;

        for (key in config.components) {
            DynamicComponent = this.availableComponents.get(key);

            if (!DynamicComponent) {
                console.error(`EntityFactory.createFrom: could not find component#${key}`);

                return;
            }

            componentConfig = config.components[key];

            entity.addComponent(new DynamicComponent(componentConfig));
        }

        return entity;
    }
}