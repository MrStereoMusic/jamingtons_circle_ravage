import { Canvas } from '../Rendering/Shared/Canvas';
import { Entity } from './Entity';

export type WorldEventArgs = {
    entity?: Entity;
    ctx?: CanvasRenderingContext2D;
}