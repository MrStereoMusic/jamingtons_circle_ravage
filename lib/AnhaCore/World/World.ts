import { AnhaCoreEvent } from '../Events/AnhaCoreEvent';
import { Camera2D } from '../Core/Camera2D';
import { Canvas } from '../Rendering/Shared/Canvas';
import { Canvas2DRenderer } from '../Rendering/Canvas2D/Canvas2DRenderer';
import { Collisions } from '../System/Collisions';
import { Entity } from './Entity';
import { EntityConfig } from '../Config/EntityConfig';
import { EntityFactory } from './Factory/EntityFactory';
import { List } from '../Collection/List';
import { Physics } from '../System/Physics';
import { SceneObject } from '../Scene/SceneObject';
import { System } from '../System/System';
import { Vector2 } from '../Physics/Vector2';
import { WorldConfig } from '../Config/WorldConfig';
import { WorldEventArgs } from './WorldEventArgs';
import { WorldRendering } from '../System/WorldRendering';

export class World extends SceneObject {

    private entities: List<Entity>;
    private entityLookup: Map<number, number>;

    public readonly size: Vector2;

    private systems: List<System>;
    private systemLookup: Map<string, number>;

    private renderingSystem: WorldRendering;
    public readonly camera: Camera2D;

    public readonly gravity: number;

    private entityFactory: EntityFactory;
    private lastEntityId: number;

    public readonly onEntityAdded: AnhaCoreEvent<WorldEventArgs>;
    public readonly onEntityRemoved: AnhaCoreEvent<WorldEventArgs>;
    public readonly onPreRender: AnhaCoreEvent<WorldEventArgs>;
    public readonly onPostRender: AnhaCoreEvent<WorldEventArgs>;

    constructor(config: WorldConfig) {
        super(config.transform);

        this.entities = new List<Entity>();
        this.entityLookup = new Map<number, number>();
        this.entityFactory = config.entityFactory || null;
        this.lastEntityId = 0;

        this.size = new Vector2(config.size.x, config.size.y);

        this.systems = new List<System>();
        this.systemLookup = new Map<string, number>();

        this.camera = config.camera;
        this.gravity = config.gravity;

        this.onEntityAdded = new AnhaCoreEvent<WorldEventArgs>();
        this.onEntityRemoved = new AnhaCoreEvent<WorldEventArgs>();
        this.onPreRender = new AnhaCoreEvent<WorldEventArgs>();
        this.onPostRender = new AnhaCoreEvent<WorldEventArgs>();

        this.renderingSystem = new WorldRendering(config.stage);
        this.setRenderer(this.renderingSystem.renderer);

        // TODO: sort systems by upcoming "order" property
        let systems: Array<System> = [
            new Physics(),
            new Collisions(),
            this.renderingSystem
        ];

        if (config.systems) {
            systems = systems.concat(config.systems);
        }

        this.initSystems(systems);
    }

    private initSystems(systems: Array<System>): void {
        let system;

        for (system of systems) {
            system.addSubscriptions(this);
            system.initialize(this);
            this.addSystem(system);
        }
    }

    public addSystem(system: System): boolean {

        if (this.systemLookup.has(system.tag)) {
            console.error(`World.addSystem: tried to add an already existing system.`);

            return false;
        }

        this.systems.add(system);
        this.systemLookup.set(system.tag, this.systems.lastIndex());

        return true;
    }

    public addEntityByConfig(config: EntityConfig): boolean {

        if (!this.entityFactory) {
            console.error(`World.addEntityByConfig: this world was not initialized with an entity factory.`);

            return false;
        }

        let entity = this.entityFactory.createFrom(config);

        this.addEntity(entity);

        return true;
    }

    public addEntity(entity: Entity): void {

        if (this.entities.contains(entity)) {
            console.info(`World.addEntity: entity was already added.`);

            return;
        }

        let id = ++this.lastEntityId;

        entity.attachID(id);

        this.entities.add(entity);
        this.entityLookup.set(id, this.entities.lastIndex());

        this.onEntityAdded.dispatch(this, {
            entity: entity
        });
    }

    public removeEntity(entity: Entity): void {

        if (!this.entities.contains(entity)) {
            console.info(`World.removeEntity: entity does not exist.`);

            return;
        }

        this.entityLookup.delete(entity.ID);
        this.entities.remove(entity);

        this.onEntityRemoved.dispatch(this, {
            entity: entity
        });
    }

    public entityById(id: number): Entity {
        let index = this.entityLookup.get(id);

        return this.entities.get(index);
    }

    public systemByTag<T extends System>(tag: string): T {
        let index = this.systemLookup.get(tag);

        return <T>this.systems.get(index);
    }

    // preUpdate, update, postUpdate van game gebruiken?
    public update(d: number): void {
        let systems = this.systems.all();
        let len = systems.length;
        let i;
        let system;

        for (i = 0; i < len; ++i) {
            system = systems[i];
            system.preUpdateSystem(d, this);
        }

        for (i = 0; i < len; ++i) {
            system = systems[i];
            system.updateSystem(d, this);
        }

        this.camera.update(d);

        for (i = 0; i < len; ++i) {
            system = systems[i];
            system.postUpdateSystem(d, this);
        }
    }

    public render(stage: Canvas): void {

        this.onPreRender.dispatch(this, {
            ctx: (<Canvas2DRenderer>this.renderer).ctx
        });

        this.renderer.render(this.renderingSystem.renderables, this.camera.transformation());

        this.onPostRender.dispatch(this, {
            ctx: (<Canvas2DRenderer>this.renderer).ctx
        });

    }

    public ignoreInput(): void {
        super.ignoreInput();

        this.entities.all().forEach(ent => {
            ent.ignoreInput();
        });
    }

    public listenToInput(): void {
        super.listenToInput();

        this.entities.all().forEach(ent => {
            ent.listenToInput();
        });
    }

    public reset(): void {
        let i;
        let systems = this.systems;
        let len = systems.length;
        let system;

        for (i = 0; i < len; ++i) {
            system = systems[i];
            system.reset();
        }
    }
}