export class ImageLoader {

    private totalImages: number;
    private loadedImages: number;
    private images: { [key: string]: HTMLImageElement };
    private callback: any;

    constructor() {
        this.totalImages = 0;
        this.loadedImages = 0;
        this.images = {};
        this.callback = null;
    }

    public setCallback(func: any): void {
        this.callback = func;
    }

    public loadImages(imagesList: { [key: string]: string }): void {
        let _onLoad = () => {
            this.onImageLoad();
        };

        let _onError = (e) => {
            this.onError(e);
        };
        let temp;

        this.loadedImages = 0;
        this.totalImages = Object.keys(imagesList).length;

        for (let image in imagesList) {
            temp = new Image();

            temp.src = imagesList[image];
            temp.onerror = _onError;
            temp.onload = _onLoad;

            this.images[image] = temp;
        }

    }

    public getImage(name: string): HTMLImageElement {
        let image = this.images[name];

        if (!image) {
            console.error(`ImageLoader.getImage: image was not found: "${name}"`);
        }

        return image;
    }

    public getProgressPercent(): number {
        return Math.floor((this.loadedImages / this.totalImages) * 100);
    }

    public onError(e: string): void {
        console.warn("AssetsLoader: there went something wrong. Error:");
        console.log(e);
    }

    private onImageLoad(): void {
        ++this.loadedImages;

        if (this.loadedImages >= this.totalImages) {
            this.callback();
        }
    }

}
