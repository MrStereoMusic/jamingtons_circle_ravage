import { Component } from '../Component';
import { ParticleCollectionConfig } from '../../Config/ParticleCollectionConfig';
import { ParticleEmitter } from '../../Particles/ParticleEmitter';

export class ParticleCollection extends Component {

    public readonly emitter: ParticleEmitter;

    constructor(config: ParticleCollectionConfig) {
        super("particleCollection");

        this.emitter = new ParticleEmitter(config.parent, config.emitter);
    }
}