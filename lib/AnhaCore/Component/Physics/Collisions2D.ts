import { BoundingBox } from '../../Physics/BoundingBox';
import { BroadphaseTestResult } from '../../Physics/Collisions/BroadphaseTestResult';
import { CollisionResponseAction } from './CollisionResponseAction';
import { Collisions2DConfig } from '../../Config/Collisions2DConfig';
import { Component } from '../Component';
import { IsCollidableAction } from './IsCollidableAction';
import { ViewportClampedAction } from './ViewportClampedAction';

export class Collisions2D extends Component {

    public readonly category: number;
    public readonly group: number;
    public readonly solid: boolean;
    public readonly bounds: BoundingBox;
    public readonly keepInVirtualViewport: boolean;
    public readonly disableSelfCheck: boolean;

    public previousBounds: BoundingBox;
    public lastCollision: BroadphaseTestResult;

    public readonly onCollisionEnter: CollisionResponseAction;
    public readonly onCollisionStay: CollisionResponseAction;
    public readonly onCollisionExit: CollisionResponseAction;
    public readonly isCollidable: IsCollidableAction;
    public readonly onVirtualViewportClamped: ViewportClampedAction;

    constructor(config: Collisions2DConfig) {
        super("collisions2d");

        config = Object.assign({
            keepInVirtualViewport: false,
            onCollisionEnter: () => { },
            onCollisionStay: () => { },
            onCollisionExit: () => { },
            isCollidable: () => { return true },
            onVirtualViewportClamped: () => { },
            disableSelfCheck: false
        }, config);

        Object.assign(this, {
            ...config,
            bounds: new BoundingBox(config.bounds)
        });
    }
}