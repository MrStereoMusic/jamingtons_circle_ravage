import { Axis } from '../../Physics/Collisions/Axis.enum';
import { BoundingBox } from '../../Physics/BoundingBox';
import { Entity } from '../../World/Entity';

export interface IsCollidableAction {
    (collider: Entity, axis: Axis, fromBounds: BoundingBox): void
};