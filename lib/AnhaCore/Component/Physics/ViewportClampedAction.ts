import { Side } from '../../Physics/Collisions/Side.enum';

export interface ViewportClampedAction {
    (side: Side): void
};