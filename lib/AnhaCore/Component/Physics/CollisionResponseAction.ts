import { Axis } from '../../Physics/Collisions/Axis.enum';
import { Entity } from '../../World/Entity';
import { Side } from '../../Physics/Collisions/Side.enum';

export interface CollisionResponseAction {
    (collider: Entity, side: Side, axis: Axis): void
};