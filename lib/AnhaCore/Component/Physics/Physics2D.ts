import { Component } from '../Component';
import { MathTool } from '../../Tool/MathTool';
import { Physics2DConfig } from '../../Config/Physics2DConfig';
import { Vector2 } from '../../Physics/Vector2';

export class Physics2D extends Component {

    public readonly acceleration: Vector2;
    public readonly velocity: Vector2;
    public readonly friction: Vector2;
    public readonly bounce: number;

    public readonly maxVelocity: Vector2;
    public readonly maxAcceleration: Vector2;
    public readonly maxFriction: Vector2;

    public readonly collidable: boolean;

    public readonly mass: number;
    public readonly floating: boolean;

    constructor(config: Physics2DConfig) {
        super("physics2d");

        config = Object.assign({
            acceleration: {
                x: 0,
                y: 0
            },
            velocity: {
                x: 0,
                y: 0
            },
            floating: false,
            collidable: true
        }, config);

        Object.assign(this, {
            velocity: new Vector2(config.velocity.x, config.velocity.y),
            acceleration: new Vector2(config.acceleration.x, config.acceleration.y),
            friction: new Vector2(0, 0),
            bounce: config.bounce,
            mass: config.mass,
            collidable: config.collidable,
            maxFriction: new Vector2(config.friction.x, config.friction.y),
            maxAcceleration: new Vector2(config.maxAcceleration.x, config.maxAcceleration.y),
            maxVelocity: new Vector2(config.maxVelocity.x, config.maxVelocity.y),
            floating: config.floating
        });

        this.bounce = MathTool.clamp(this.bounce, 0, 0.9);
    }
}