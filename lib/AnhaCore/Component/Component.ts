export abstract class Component {

    public readonly tag: string;

    constructor(tag: string) {
        this.tag = tag;
    }
}