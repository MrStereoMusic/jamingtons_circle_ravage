import { TweenAnimations } from './TweenAnimations';

export interface SetTweenAnimationAction {
    (animations: TweenAnimations): void
};