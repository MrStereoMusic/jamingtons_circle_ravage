import { TweenConfigData } from './TweenConfigData';

export interface TweenConfigCollection {
    retrieveTweenConfigs(): TweenConfigData;
}