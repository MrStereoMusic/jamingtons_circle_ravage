import { Component } from '../../Component';
import { Entity } from '../../../World/Entity';
import { EventArgs } from '../../../Events/EventArgs';
import { SetTweenAnimationAction } from './SetTweenAnimationAction';
import { Tween } from '../../../Animation/Tweening/Tween';
import { TweenAnimationsConfig } from '../../../Config/TweenAnimationsConfig';
import { TweenConfig } from '../../../Config/TweenConfig';
import { Tweens } from '../../../Animation/Tweening/Tweens';

export class TweenAnimations extends Component {

    private currentAnimKey: string;
    public readonly animationSet: Map<string, Array<TweenConfig>>;
    public readonly tweens: Tweens;
    public readonly setAnimation: SetTweenAnimationAction;

    constructor(config: TweenAnimationsConfig) {
        super("tweenAnimations");

        config = Object.assign({
            setAnimation: null,
        }, config);

        this.tweens = new Tweens();

        if (config.tweenCollection) {

            this.currentAnimKey = "";
            this.animationSet = new Map<string, Array<TweenConfig>>();

            let tweenConfigs = config.tweenCollection.retrieveTweenConfigs();

            for (let animKey in tweenConfigs) {
                this.animationSet.set(animKey, tweenConfigs[animKey]);
            }

            this.setAnimation = config.setAnimation;
        }
    }

    public initialize(entity?: Entity): void {
        //
    }

    public playAnimation(key: string, force: boolean = false): void {

        if (!this.animationSet.has(key)) {
            console.error(`TweenAnimations.playAnimation: unknown animation "${key}"`);

            return;
        }

        if (this.currentAnimKey == key) {
            return;
        }

        if (!this.tweens.animating() || force) {
            this.currentAnimKey = key;
            this.initializeTweens(this.animationSet.get(key));
            this.unpause();
        }
    }

    private initializeTweens(tweenConfigs: Array<TweenConfig>): void {
        this.tweens.clear();

        let i;
        let config;
        let tween;
        for (i = 0; i < tweenConfigs.length; ++i) {
            config = tweenConfigs[i];
            tween = new Tween(config);

            if (i == tweenConfigs.length - 1) {
                tween.onFinished.subscribe(this, this.onTweenSequenceFinished);
            }

            this.tweens.add(tween);
        }
    }

    private onTweenSequenceFinished(sender: Tween, args: EventArgs): void {
        this.resetKey();
    }

    public resetKey(): void {
        this.currentAnimKey = "";
    }

    public pause(): void {
        this.tweens.pause();
    }

    public unpause(): void {
        this.tweens.unpause();
    }
}