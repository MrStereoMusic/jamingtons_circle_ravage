import { TweenConfig } from '../../../Config/TweenConfig';

export type TweenConfigData = {
    [key: string]: Array<TweenConfig>;
};