import { SpriteAnimations } from './SpriteAnimations';

export interface SetSpriteAnimationAction {
    (animations: SpriteAnimations): void
};