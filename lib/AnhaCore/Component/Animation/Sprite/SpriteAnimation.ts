import { Vector2Config } from '../../../Config/Vector2Config';

export interface SpriteAnimation {
    readonly frameSize: Vector2Config;
    readonly baseFrame: Vector2Config;
    readonly rows: number;
    readonly columns: number;
    readonly fps: number;
    readonly loop: boolean;
}