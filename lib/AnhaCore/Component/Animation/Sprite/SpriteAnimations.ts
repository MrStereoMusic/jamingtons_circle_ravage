import { Component } from '../../Component';
import { Entity } from '../../../World/Entity';
import { SetSpriteAnimationAction } from './SetSpriteAnimationAction';
import { SpriteAnimation } from './SpriteAnimation';
import { SpriteAnimationsConfig } from '../../../Config/SpriteAnimationsConfig';

export class SpriteAnimations extends Component {

    public readonly animationSet: Map<string, SpriteAnimation>;

    public currentAnimKey: string;
    public nextAnimKey: string;

    public currentColumn: number;
    public currentRow: number;

    public running: boolean;
    public tickSpeed: number;
    public accumulator: number;

    public readonly setAnimation: SetSpriteAnimationAction;

    constructor(config: SpriteAnimationsConfig) {
        super("spriteAnimations");

        config = Object.assign({
            setAnimation: () => { },
        }, config);

        this.animationSet = new Map<string, SpriteAnimation>();

        for (let animKey in config.animationSet) {
            this.animationSet.set(animKey, config.animationSet[animKey]);
        }

        this.setAnimation = config.setAnimation;

        this.currentColumn = 0;
        this.currentRow = 0;
        this.currentAnimKey = "";
        this.nextAnimKey = "";
        this.running = false;
        this.tickSpeed = 0;
        this.accumulator = 0;
    }

    public initialize(entity?: Entity): void {
        //
    }

    public playAnimation(key: string): void {

        if (!this.animationSet.has(key)) {
            console.error(`SpriteAnimations.playAnimation: unknown animation "${key}"`);

            return;
        }

        if (this.currentAnimKey == key) {
            return;
        }

        this.unpause();
        this.reset();

        this.currentAnimKey = key;
        this.tickSpeed = this.animationSet.get(key).fps;
    }

    public playOnce(key: string): void {
        this.playAnimation(key);
        this.nextAnimKey = this.currentAnimKey;
    }

    public pause(): void {
        this.running = false;
    }

    public unpause(): void {
        this.running = true;
    }

    public reset(): void {
        this.currentColumn = 0;
        this.currentRow = 0;
        this.accumulator = 0;
    }
}