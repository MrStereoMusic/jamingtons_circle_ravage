import { SpriteConfig } from '../../Config/SpriteConfig';
import { SpriteRendererC2D } from '../../Rendering/Canvas2D/SpriteRendererC2D';
import { Transform2D } from './Transform2D';
import { Vector2 } from '../../Physics/Vector2';
import { View2D } from './View2D';

export class Sprite extends View2D {

    public readonly image: HTMLImageElement | HTMLCanvasElement;
    public readonly frame: Vector2;

    constructor(config: SpriteConfig) {
        let stage = config.stage;
        let renderer;

        if (stage.isWebGL) {
           //renderer = new SpriteRendererGL2D(stage);
        } else {
            renderer = new SpriteRendererC2D(stage);
        }

        super({ renderer });

        this.image = config.image;
        this.frame = new Vector2(config.frame.x, config.frame.y);
    }
}