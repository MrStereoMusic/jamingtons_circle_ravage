import { Component } from '../Component';
import { Renderer } from '../../Rendering/Shared/Renderer';
import { View2DConfig } from '../../Config/View2DConfig';

export abstract class View2D extends Component {

    public readonly renderer: Renderer;

    constructor(config: View2DConfig) {
        super("view2d");

        this.renderer = config.renderer;
    }
}

