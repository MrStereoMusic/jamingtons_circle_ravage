import { CircleConfig } from '../../Config/CircleConfig';
import { CircleRendererC2D } from '../../Rendering/Canvas2D/CircleRendererC2D';
import { Color } from '../../UI/Color';
import { View2D } from './View2D';

export class Circle extends View2D {

    public color: Color;
    public radius: number;

    constructor(config: CircleConfig) {
        let stage = config.stage;
        let renderer;

        if (stage.isWebGL) {
           // nothing here
        } else { 
            renderer = new CircleRendererC2D(stage);
        }

        super({ renderer });

        this.color = new Color(config.color);
        this.radius = config.radius;
    }
}