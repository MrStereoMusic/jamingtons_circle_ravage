import { Component } from '../Component';
import { EffectsConfig } from '../../Config/EffectsConfig';
import { MathTool } from '../../Tool/MathTool';
import { Matrix2D } from '../../Rendering/Shared/Matrix2D';
import { Origin } from './Origin.enum';
import { Transform2DConfig } from '../../Config/Transform2DConfig';
import { Vector2 } from '../../Physics/Vector2';

export class Transform2D extends Component {

    public position: Vector2;
    public origin: Vector2;
    public translation: Vector2;
    public scale: Vector2;
    public angle: number;
    public width: number;
    public height: number;
    public zIndex: number;
    public effects: EffectsConfig;

    public readonly matrix: Matrix2D;

    constructor(config: Transform2DConfig) {
        super("transform2d");

        let effectsConfig = Object.assign({
            blur: 1, // TODO: remove
            mirrorX: false,
            mirrorY: false,
            opacity: 1
        }, config.effects);

        config = Object.assign({
            position: {
                x: 0,
                y: 0
            },
            translation: {
                x: 0,
                y: 0
            },
            origin: {
                x: 0,
                y: 0
            },
            scale: {
                x: 1,
                y: 1
            },
            angle: 0,
            width: config.width,
            height: config.height,
            zIndex: 0
        }, config);

        this.width = config.width;
        this.height = config.height;
        this.translation = new Vector2(config.translation.x, config.translation.y);
        this.angle = config.angle;
        this.zIndex = config.zIndex;
        this.position = new Vector2(config.position.x, config.position.y);
        this.origin = new Vector2(config.origin.x, config.origin.y);
        this.scale = new Vector2(config.scale.x, config.scale.y);
        this.effects = effectsConfig;
        this.matrix = new Matrix2D();
    }

    public setOrigin(origin: Origin): void {
        let x = 0;
        let y = 0;

        switch (origin) {
            case Origin.CENTER:
                x = this.width * .5;
                y = this.height * .5;
                break;
            case Origin.TOPRIGHT:
                x += this.width;
                break;
            case Origin.BOTTOMLEFT:
                y += this.height;
                break;
            case Origin.BOTTOMRIGHT:
                x += this.width;
                y += this.height;
                break;
            default: break;
        }

        this.origin.x = x;
        this.origin.y = y;
    }

    public computeMatrix(viewportX: number, viewportY: number): Matrix2D {
        let matrix = this.matrix;

        matrix.project(viewportX, viewportY);

        matrix.translate(
            Math.round(this.position.x + this.translation.x + this.origin.x),
            Math.round(this.position.y + this.translation.y + this.origin.y)
        );
        matrix.rotate(MathTool.degToRad(this.angle));
        matrix.scale(this.scale.x, this.scale.y);
        matrix.translate(-this.origin.x, -this.origin.y);

        return matrix;
    }

    public centerX(): number {
        return this.width * .5;
    }

    public centerY(): number {
        return this.height * .5;
    }

    public center(): Vector2 {
        return new Vector2(this.centerX(), this.centerY());
    }

    public centerOn(parentPosition: Vector2, parentWidth: number, parentHeight: number): void {
        this.position.x = ~~(parentPosition.x + (parentWidth * .5)) - ~~(this.position.x + (this.width * .5));
        this.position.y = ~~(parentPosition.y + (parentHeight * .5)) - ~~(this.position.y + (this.height * .5));
    }
}