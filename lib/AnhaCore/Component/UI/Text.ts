// import { TextConfig } from '../../Config/TextConfig';
// import { View2D } from './View2D';

// export class Text extends View2D {

//     public label: string;
//     public lineHeight: number;
//     public font: string;
//     public color: string;

//     constructor(config: TextConfig) {
//         let stage = config.stage;
//         let renderer;

//         if (stage.isWebGL) {
//             renderer = new SpriteRendererGL2D(stage);
//         } else {
//             renderer = new SpriteRendererC2D(stage);
//         }

//         super({ renderer });

//         this.label = config.label;
//         this.lineHeight = config.lineHeight;
//         this.font = config.font;
//         this.color = config.color;
//     }
// }