import { Color } from '../../UI/Color';
import { RectangleConfig } from '../../Config/RectangleConfig';
import { RectangleRendererC2D } from '../../Rendering/Canvas2D/RectangleRendererC2D';
import { Transform2D } from './Transform2D';
import { View2D } from './View2D';

export class Rectangle extends View2D {

    public color: Color;

    constructor(config: RectangleConfig) {
        let stage = config.stage;
        let renderer;

        if (stage.isWebGL) {
            //renderer = new RectangleRendererGL2D(stage);
        } else {
            renderer = new RectangleRendererC2D(stage);
        }

        super({ renderer });

        this.color = new Color(config.color);
    }
}