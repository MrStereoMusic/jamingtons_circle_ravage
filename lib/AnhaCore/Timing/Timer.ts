import { Action } from '../Core/Action';
import { TimerConfig } from '../Config/TimerConfig';
import { Updatable } from '../Core/Updatable';

export class Timer implements Updatable {

    private accumulator: number;
    private interval: number;
    private action: Action;

    private running: boolean;

    constructor(config: TimerConfig) {
        this.accumulator = 0;
        this.interval = config.interval;
        this.action = config.action;
        this.running = false;
    }

    get currentTime(): number {
        return this.accumulator;
    }

    public start(): void {
        this.reset();
        this.accumulator = 0;
        this.running = true;
    }

    public stop(): void {
        this.running = false;
        this.reset();
    }

    public pause(): void {
        this.running = false;
    }

    public unpause(): void {
        this.running = true;
    }

    public reset(): void {
        this.accumulator = 0;
    }

    public update(d: number): void {

        if (this.running) {

            this.accumulator += d;

            if (this.accumulator > this.interval) {
                this.action();
                this.reset();
            }
        }
    }
}