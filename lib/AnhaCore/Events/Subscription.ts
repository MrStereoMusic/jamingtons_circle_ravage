import { EventDelegate } from './EventDelegate';

export interface Subscription<T, S, A> {
    object: T,
    method: EventDelegate<S, A>
}