export interface EventDelegate<S, A> {
    (sender: S, ...args: Array<A>): void; // eventargs
}
