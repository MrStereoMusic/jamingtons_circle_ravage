import { EventDelegate } from './EventDelegate';
import { List } from './../Collection/List';
import { Subscription } from './Subscription';

export class AnhaCoreEvent<A> {

    private listeners: List<Subscription<any, any, A>>;

    constructor() {
        this.listeners = new List<Subscription<any, any, A>>();
    }

    public subscribe(object: any, callback: EventDelegate<any, A>): void {
        let subscription = {
            object: object,
            method: callback
        };

        this.listeners.add(subscription);
    }

    public unsubscribe(object: any): void {
        let ltr: Array<Subscription<any, any, A>> = this.listeners.where((s) => (s.object === object));

		// wat als het object niet bestaat?

        this.listeners.remove(ltr[0]);
    }

    public dispatch<S, A>(sender: S, eventArgs?: A): void {
        let listeners = this.listeners.all();

        listeners.forEach(ltr => {

            try {
                ltr.method.call(ltr.object, sender, eventArgs);
            } catch (e) {
                console.error(e);
            }
        });
    }
}