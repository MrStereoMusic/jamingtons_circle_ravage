import { AnhaCoreEvent } from '../Events/AnhaCoreEvent';
import { AnhaCoreGame } from '../Core/AnhaCoreGame';
import { Client } from '../Core/Client';
import { EventArgs } from '../Events/EventArgs';
import { InputDevice } from './InputDevice';
import { MathTool } from '../Tool/MathTool';
import { MouseButton } from './MouseButton.enum';
import { Vector2 } from '../Physics/Vector2';

export class Mouse extends InputDevice {

    public readonly position: Vector2;

    private client: Client;
    private _mouseWheelDelta: number;

    private currentActiveButtons: { [key: string]: boolean };
    private previousActiveButtons: { [key: string]: boolean };

    public readonly onContextMenu: AnhaCoreEvent<MouseEvent>;
    public readonly onButtonUp: AnhaCoreEvent<MouseEvent>;
    public readonly onButtonDown: AnhaCoreEvent<MouseEvent>;
    public readonly onMove: AnhaCoreEvent<MouseEvent>;
    public readonly onWheel: AnhaCoreEvent<WheelEvent>;

    constructor(client: Client) {
        super("mouse");

        this.client = client;
        this.position = new Vector2(0, 0);
        this.currentActiveButtons = {};
        this.previousActiveButtons = {};

        this.onContextMenu = new AnhaCoreEvent<MouseEvent>();
        this.onButtonUp = new AnhaCoreEvent<MouseEvent>();
        this.onButtonDown = new AnhaCoreEvent<MouseEvent>();
        this.onMove = new AnhaCoreEvent<MouseEvent>();
        this.onWheel = new AnhaCoreEvent<WheelEvent>();
    }

    public initEvents(): void {

        window.addEventListener("contextmenu", (e) => {
            e.preventDefault();
            e.stopPropagation();

            this.onContextMenu.dispatch(this, e);
        }, false);

        window.addEventListener("mouseup", (e: MouseEvent) => {
            this.currentActiveButtons[e.button] = false;

            this.onButtonUp.dispatch(this, e);
        }, false);

        window.addEventListener("mousedown", (e: MouseEvent) => {
            this.currentActiveButtons[e.button] = true;

            this.onButtonDown.dispatch(this, e);
        }, false);

        window.addEventListener("mousewheel", (e: WheelEvent) => {
            e.preventDefault();
            e.stopPropagation();

            this._mouseWheelDelta = e.wheelDelta;

            this.onWheel.dispatch(this, e);
        }, false);

        this.client.stage.domElement.addEventListener("mousemove", (e: MouseEvent) => {
            this.localize(e);

            this.onMove.dispatch(this, e);
        }, false);
    }

    get wheelDelta(): number {
        return MathTool.clamp(this._mouseWheelDelta, -1, 1);
    }

    private localize(e): Vector2 {
        let stage = this.client.stage;
        let bounds = stage.domElement.getBoundingClientRect();
        let ratio = this.client.ratio;

        this.position.x = Math.round((e.clientX - bounds.left) * (1 / ratio));
        this.position.y = Math.round((e.clientY - bounds.top) * (1 / ratio));

        return this.position;
    }

    public onPostUpdate(sender: AnhaCoreGame, e: EventArgs): void {
        let key;

        for (key in this.currentActiveButtons) {
            this.previousActiveButtons[key] = this.currentActiveButtons[key];
        }

    }

    public isButtonHeld(button: MouseButton): boolean {
        return this.currentActiveButtons[button];
    }

    public isButtonDown(button: MouseButton): boolean {
        return this.currentActiveButtons[button] && (this.currentActiveButtons[button] != this.previousActiveButtons[button]);
    }
}