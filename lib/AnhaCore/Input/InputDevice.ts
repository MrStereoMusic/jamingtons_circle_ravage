import { AnhaCoreGame } from '../Core/AnhaCoreGame';
import { EventArgs } from '../Events/EventArgs';

export abstract class InputDevice {

    public readonly tag: string;

    constructor(tag: string) {
        this.tag = tag;
    }

    abstract initEvents(): void;
    abstract onPostUpdate(sender: AnhaCoreGame, e: EventArgs): void;
}