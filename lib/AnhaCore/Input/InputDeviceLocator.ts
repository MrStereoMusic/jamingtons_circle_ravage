import { AnhaCoreGame } from '../Core/AnhaCoreGame';
import { EventArgs } from '../Events/EventArgs';
import { InputDevice } from './InputDevice';
import { List } from '../Collection/List';
import { Updatable } from '../Core/Updatable';

export class InputDeviceLocator {

    private devices: List<InputDevice>;
    private deviceLookup: Map<string, number>;

    constructor(game: AnhaCoreGame) {
        this.devices = new List<InputDevice>();
        this.deviceLookup = new Map<string, number>();

        game.onPostUpdate.subscribe(this, this.onPostUpdate);
    }

    public addDevice<T extends InputDevice>(object: T): void {

        if (this.hasDevice(object.tag)) {
            console.error(`InputDeviceLocator.addDevice: device of type ${object + ""} already exists.`);

            return;
        }

        object.initEvents();

        this.devices.add(object);
        this.deviceLookup.set(object.tag, this.devices.lastIndex());
    }

    public getDevice<T extends InputDevice>(tag: string): T {

        if (!this.hasDevice(tag)) {
            console.error(`InputDeviceLocator.getDevice: device of type ${<T>{}} does not exists.`);

            return;
        }

        let index = this.deviceLookup.get(tag);
        let device = this.devices.get(index);

        return <T>device;
    }

    private hasDevice(tag: string): boolean {
        return this.deviceLookup.has(tag);
    }

    private onPostUpdate(sender: AnhaCoreGame, e: EventArgs): void {
        let devices = this.devices.all();

        devices.forEach(device => {
            device.onPostUpdate(sender, e);
        });
    }
}