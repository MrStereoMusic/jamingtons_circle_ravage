import { AnhaCoreEvent } from '../Events/AnhaCoreEvent';
import { AnhaCoreGame } from '../Core/AnhaCoreGame';
import { EventArgs } from '../Events/EventArgs';
import { InputDevice } from './InputDevice';
import { Key } from './Key.enum';

export class Keyboard extends InputDevice {

    private currentActiveKeys: { [key: string]: boolean };
    private previousActiveKeys: { [key: string]: boolean };

    public readonly onKeyDown: AnhaCoreEvent<EventArgs>;
    public readonly onKeyUp: AnhaCoreEvent<EventArgs>;

    constructor(game: AnhaCoreGame) {
        super("keyboard");

        this.currentActiveKeys = {};
        this.previousActiveKeys = {};

        this.onKeyDown = new AnhaCoreEvent<KeyboardEvent>();
        this.onKeyUp = new AnhaCoreEvent<KeyboardEvent>();

        game.onPreUpdate.subscribe(this, this.onPreUpdate);
    }

    public initEvents(): void {

        window.addEventListener("keydown", (e) => {
            this.currentActiveKeys[e.keyCode] = true;
        }, false);

        window.addEventListener("keyup", (e) => {
            this.currentActiveKeys[e.keyCode] = false;
        }, false);

        window.addEventListener("blur", this.emptyKeys);
    }

    private emptyKeys(): void {
        this.currentActiveKeys = {};
        this.previousActiveKeys = {};
    }

    public onPreUpdate(sender: AnhaCoreGame, e: EventArgs): void {
        this.onKeyDown.dispatch(this, e);
        this.onKeyUp.dispatch(this, e);
    }

    public onPostUpdate(sender: AnhaCoreGame, e: EventArgs): void {
        let key;

        for (key in this.currentActiveKeys) {
            this.previousActiveKeys[key] = this.currentActiveKeys[key];
        }
    }

    public isKeyDown(key: Key): boolean {
        return this.currentActiveKeys[key] && (this.currentActiveKeys[key] != this.previousActiveKeys[key]);
    }

    public isKeyHeld(key: Key): boolean {
        return this.currentActiveKeys[key];
    }

    public isKeyUp(key: Key): boolean {
        return this.previousActiveKeys[key] != this.currentActiveKeys[key];
    }
}


