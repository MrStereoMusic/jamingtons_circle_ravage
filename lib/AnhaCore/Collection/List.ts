import { ListAction } from './ListAction';
import { MathTool } from '../Tool/MathTool';

export class List<T> {
    private children: Array<T>;

    constructor(objects?: Array<T>) {
        this.children = [];

        if (typeof objects != "undefined") {
            for (let object of objects) {
                this.add(object);
            }
        }
    }

    get length(): number {
        return this.children.length;
    }

    public count(): number {
        return this.children.length;
    }

    public add(child: T): T {
        this.children.push(child);

        return child;
    }

    public get(index: number): T {
        return this.children[index];
    }

    public all(): Array<T> {
        return this.children;
    }

    public remove(object: T): void {
        let index = this.findIndex(object);

        if (index != -1) {
            this.children.splice(index, 1);
        }
    }

    public removeLast(): T {
        return this.children.pop();
    }

    public clear(): void {
        this.children.length = 0;
    }

    public contains(object: T): boolean {
        return (this.findIndex(object) != -1);
    }

    public hasIndex(index: number): boolean {
        return (index >= 0 && index < this.children.length);
    }

    public merge(array: List<T>): void {
        this.children.push.apply(this.children, array);
    }

    public sort(comparison: ListAction<T>): void {
        this.children = this.children.sort(comparison);
    }

    public where(filter: (value: T, index?: number, array?: Array<T>) => any): Array<T> {
        return this.children.filter(filter);
    }

    public swap(from: number, to: number): void {
        let object = this.children[from];

        this.children[from] = this.children[to];
        this.children[to] = object;
    }

    public prevOrNull(index: number): T & null {
        return (this.hasIndex(index - 1) ? this.children[index - 1] : null);
    }

    public nextOrNull(index: number): T & null {
        return (this.hasIndex(index + 1) ? this.children[index + 1] : null);
    }

    public getFirst(): T {
        return this.children[0];
    }

    public getLast(): T {
        return this.children[this.children.length - 1];
    }

    public findIndex(object: T): number {
        return this.children.indexOf(object);
    }

    public lastIndex(): number {
        return (this.children.length - 1);
    }

    public rand(): T {
        let i = Math.floor(MathTool.rand(0, this.children.length));

        return this.get(i);
    }
}
