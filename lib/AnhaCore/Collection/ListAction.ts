export interface ListAction<F> {
    (first: F, second?: F): any
}