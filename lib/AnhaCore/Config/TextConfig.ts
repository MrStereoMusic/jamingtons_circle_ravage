import { Canvas } from '../Rendering/Shared/Canvas';
import { Transform2DConfig } from './Transform2DConfig';
import { View2D } from '../Component/UI/View2D';

export type TextConfig = {
    label: string;
    lineHeight: number;
    font: string;
    color: string;
    transform: Transform2DConfig;
    stage: Canvas;
}