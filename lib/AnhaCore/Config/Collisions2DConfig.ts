import { BoundingBoxConfig } from './BoundingBoxConfig';
import { CollisionResponseAction } from '../Component/Physics/CollisionResponseAction';
import { IsCollidableAction } from '../Component/Physics/IsCollidableAction';
import { ViewportClampedAction } from '../Component/Physics/ViewportClampedAction';

export type Collisions2DConfig = {
    category: number;
    group: number;
    bounds: BoundingBoxConfig;
    solid: boolean;
    keepInVirtualViewport?: boolean;
    disableSelfCheck?: boolean;
    onCollisionEnter?: CollisionResponseAction;
    onCollisionStay?: CollisionResponseAction;
    onCollisionExit?: CollisionResponseAction;
    isCollidable?: IsCollidableAction;
    onVirtualViewportClamped?: ViewportClampedAction;
};