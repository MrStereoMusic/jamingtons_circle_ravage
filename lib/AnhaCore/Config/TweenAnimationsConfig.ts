import { SetTweenAnimationAction } from '../Component/Animation/Tweening/SetTweenAnimationAction';
import { TweenConfigCollection } from '../Component/Animation/Tweening/TweenConfigCollection';

export type TweenAnimationsConfig = {
    tweenCollection?: TweenConfigCollection;
    setAnimation?: SetTweenAnimationAction;
};