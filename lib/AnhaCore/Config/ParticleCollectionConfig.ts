import { ParticleEmitterConfig } from './ParticleEmitterConfig';
import { Transform2D } from '../Component/UI/Transform2D';

export type ParticleCollectionConfig = {
    parent: Transform2D;
    emitter: ParticleEmitterConfig;
};