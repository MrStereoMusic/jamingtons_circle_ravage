import { Vector2Config } from './Vector2Config';

export type Physics2DConfig = {
    maxVelocity: Vector2Config;
    velocity?: Vector2Config;
    maxAcceleration?: Vector2Config;
    acceleration?: Vector2Config;
    friction: Vector2Config;
    bounce: number;
    mass: number;
    floating: boolean;
    collidable?: boolean;
};