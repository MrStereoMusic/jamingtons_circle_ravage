import { PaddingConfig } from './PaddingConfig';

export type BoundingBoxConfig = {
    min: {
        x: number;
        y: number;
    };
    max: {
        x: number;
        y: number;
    };
    padding?: PaddingConfig;
}