import { BoundingBoxConfig } from './BoundingBoxConfig';
import { World } from '../World/World';

export type ParticleEmitterConfig = {
    enabled: boolean;
    particleCtor: any;
    particleConfig: any;
    maxParticles: number;
    spawnRate: number;
    bounds: BoundingBoxConfig;
}