import { Action } from '../Core/Action';

export type TimerConfig = {
    interval: number;
    action: Action;
}