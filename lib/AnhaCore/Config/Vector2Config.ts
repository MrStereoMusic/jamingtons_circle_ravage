export type Vector2Config = {
    x: number;
    y: number;
}