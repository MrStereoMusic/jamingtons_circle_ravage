export type SystemConfig = {
    tag: string;
    requiredComponents: Array<string>;
}