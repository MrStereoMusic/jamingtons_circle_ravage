import { SpriteAnimationsConfig } from './SpriteAnimationsConfig';

export type SpritesheetConfig = {
    animationSet: { [key: string]: SpriteAnimationsConfig };
}