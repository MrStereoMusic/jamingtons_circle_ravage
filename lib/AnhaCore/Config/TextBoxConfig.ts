import { TextConfig } from './TextConfig';
import { Transform2DConfig } from './Transform2DConfig';

export type TextBoxConfig = {
    text: TextConfig;
    transform?: Transform2DConfig;
};