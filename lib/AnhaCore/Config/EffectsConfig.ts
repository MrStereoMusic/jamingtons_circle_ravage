export type EffectsConfig = {
    blur?: number;
    mirrorX?: boolean;
    mirrorY?: boolean;
    opacity?: number;
};