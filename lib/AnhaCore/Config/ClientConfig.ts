export type ClientConfig = {
    stageId: string;
    resolutionX: number;
    resolutionY: number;
    enableWebGL: boolean;
}