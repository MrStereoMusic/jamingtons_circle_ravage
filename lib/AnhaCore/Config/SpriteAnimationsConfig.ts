import { SetSpriteAnimationAction } from '../Component/Animation/Sprite/SetSpriteAnimationAction';
import { SpriteAnimation } from '../Component/Animation/Sprite/SpriteAnimation';

export type SpriteAnimationsConfig = {
    animationSet: {[key: string]: SpriteAnimation};
    setAnimation?: SetSpriteAnimationAction;
};