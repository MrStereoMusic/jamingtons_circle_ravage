import { EffectsConfig } from './EffectsConfig';
import { Vector2Config } from './Vector2Config';

export type Transform2DConfig = {
    position?: Vector2Config;
    origin?: Vector2Config;
    scale?: Vector2Config;
    translation?: Vector2Config;
    angle?: number;
    width: number;
    height: number;
    effects?: EffectsConfig;
    zIndex?: number;
}