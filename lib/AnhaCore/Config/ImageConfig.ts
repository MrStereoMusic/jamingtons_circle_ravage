import { SpriteConfig } from './SpriteConfig';
import { Transform2DConfig } from './Transform2DConfig';

export type ImageConfig = {
    sprite: SpriteConfig;
    transform?: Transform2DConfig;
};