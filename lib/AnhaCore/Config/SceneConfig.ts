import { ScenePredecessorSettings } from '../Scene/ScenePredecessorSettings';

export type SceneConfig = {
    tag: string;
    bgColor?: Array<number>;
    predecessorSettings: ScenePredecessorSettings;
}