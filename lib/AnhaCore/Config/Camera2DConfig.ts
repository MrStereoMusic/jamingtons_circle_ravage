import { BoundingBoxConfig } from './BoundingBoxConfig';
import { Vector2Config } from './Vector2Config';

export type Camera2DConfig = {
    viewport: {
        width: number;
        height: number;
    };
    bounds: BoundingBoxConfig;
    focus: Vector2Config;
    smoothness: number;
};