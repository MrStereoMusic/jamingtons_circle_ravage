import { ParticleEmitterConfig } from './ParticleEmitterConfig';
import { Transform2DConfig } from './Transform2DConfig';

export type ParticleSourceConfig = {
    emitter: ParticleEmitterConfig;
    transform?: Transform2DConfig;
};