import { Physics2DConfig } from './Physics2DConfig';
import { Transform2DConfig } from './Transform2DConfig';
import { View2D } from '../Component/UI/View2D';

export type ParticleConfig = {
    life: number;
    view: View2D;
    transform: Transform2DConfig;
    physics: Physics2DConfig;
}