import { Canvas } from '../Rendering/Shared/Canvas';

export type CircleConfig = {
    color: Array<number>;
    radius: number;
    stage: Canvas;
}