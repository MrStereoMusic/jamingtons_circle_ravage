import { Canvas } from '../Rendering/Shared/Canvas';
import { Color } from '../UI/Color';

export type RectangleConfig = {
    color: Array<number>;
    stage: Canvas;
}