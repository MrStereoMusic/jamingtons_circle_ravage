export type AnhaCoreGameConfig = {
    readonly gameName: string;
    readonly resolutionX: number;
    readonly resolutionY: number;
    readonly initSceneCode: string;
    readonly developerMode: boolean;
    readonly imageSmoothing: boolean;
    readonly enableWebGL: boolean;
    readonly images: { [key: string]: string };
    readonly developer: string;
    readonly copyright: string;
    readonly version: string;
}