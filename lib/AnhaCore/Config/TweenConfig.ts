import { Action } from '../Core/Action';
import { TransitionDelegate } from '../Animation/Tweening/Transition/TransitionDelegate';
import { TweenValueChange } from '../Animation/Tweening/TweenValueChange';

export type TweenConfig = {
    init?: Action;
    properties: Array<TweenValueChange>;
    duration: number;
    easing?: TransitionDelegate;
    delay?: number;
    callback?: Action;
    loop?: boolean;
};