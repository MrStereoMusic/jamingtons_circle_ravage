export type CanvasConfig = {
    name: string;
    width: number;
    height: number;
    enableWebGL?: boolean;
}