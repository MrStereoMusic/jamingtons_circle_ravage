export type PaddingConfig = {
    top: number;
    right: number;
    bottom: number;
    left: number;
}