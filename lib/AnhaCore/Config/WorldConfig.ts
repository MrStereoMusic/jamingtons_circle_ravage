import { AnhaCoreGame } from '../Core/AnhaCoreGame';
import { Camera2D } from '../Core/Camera2D';
import { Canvas } from '../Rendering/Shared/Canvas';
import { EntityFactory } from '../World/Factory/EntityFactory';
import { System } from '../System/System';
import { Transform2D } from '../Component/UI/Transform2D';
import { Vector2 } from '../Physics/Vector2';
import { Vector2Config } from './Vector2Config';

export type WorldConfig = {
    camera: Camera2D;
    stage: Canvas;
    gravity: number;
    systems?: Array<System>;
    size: Vector2Config;
    transform?: Transform2D;
    entityFactory?: EntityFactory;
};