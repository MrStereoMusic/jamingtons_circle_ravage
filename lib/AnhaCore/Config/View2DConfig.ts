import { Renderer } from '../Rendering/Shared/Renderer';
import { View2D } from '../Component/UI/View2D';

export type View2DConfig = {
    renderer: Renderer;
};