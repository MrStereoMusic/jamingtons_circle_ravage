import { Canvas } from '../Rendering/Shared/Canvas';
import { Transform2DConfig } from './Transform2DConfig';
import { Vector2Config } from './Vector2Config';
import { View2D } from '../Component/UI/View2D';

export type SpriteConfig = {
    image: HTMLImageElement | HTMLCanvasElement;
    frame: Vector2Config;
    stage: Canvas;
}