import { EffectsConfig } from '../Config/EffectsConfig';

export interface ScenePredecessorSettings {
    pause: boolean;
    effects?: EffectsConfig;
};