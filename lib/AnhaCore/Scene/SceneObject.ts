import { Canvas } from '../Rendering/Shared/Canvas';
import { Drawable } from '../Core/Drawable';
import { Inputable } from '../Core/Inputable';
import { Renderer } from '../Rendering/Shared/Renderer';
import { Transform2D } from '../Component/UI/Transform2D';
import { Transform2DConfig } from '../Config/Transform2DConfig';
import { Updatable } from '../Core/Updatable';

export abstract class SceneObject extends Inputable implements Drawable, Updatable {

    public transform: Transform2D;
    private _renderer: Renderer;

    constructor(transform?: Transform2DConfig) {
        super();

        this.transform = new Transform2D(transform || {
            width: 0,
            height: 0
        });
    }

    protected setRenderer(renderer: Renderer): void {
        this._renderer = renderer;
    }

    get renderer(): Renderer {
        return this._renderer;
    }

    abstract update(d: number): void;
    abstract render(stage: Canvas, ...args: any[]): void;
}