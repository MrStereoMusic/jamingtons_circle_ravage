import { AnhaCoreEvent } from '../Events/AnhaCoreEvent';
import { Canvas } from '../Rendering/Shared/Canvas';
import { Color } from '../UI/Color';
import { EffectsConfig } from '../Config/EffectsConfig';
import { EventArgs } from '../Events/EventArgs';
import { jcrGame } from '../../../src/Main/JCR';
import { List } from '../Collection/List';
import { MathTool } from '../Tool/MathTool';
import { Rectangle } from '../Component/UI/Rectangle';
import { Renderer } from '../Rendering/Shared/Renderer';
import { RGBA } from './../UI/RGBA';
import { SceneConfig } from '../Config/SceneConfig';
import { SceneObject } from './SceneObject';
import { ScenePredecessorSettings } from './ScenePredecessorSettings';
import { SceneRendererC2D } from '../Rendering/Canvas2D/SceneRendererC2D';
import { Transform2D } from '../Component/UI/Transform2D';
import { Tweens } from '../Animation/Tweening/Tweens';

export abstract class Scene {

    public readonly tag: string;
    public readonly predecessorSettings: ScenePredecessorSettings;
    public initialized: boolean;

    protected bgColor: Color;

    public clearScreen: boolean;

    protected _tickSpeed: number;
    private accumulator: number;

    private children: List<SceneObject>;

    protected transitions: Tweens;
    protected transform: Transform2D;

    protected acceptInput: boolean;

    private running: boolean;
    private active: boolean;

    private renderer: Renderer;

    public readonly onReseting: AnhaCoreEvent<EventArgs>;
    public readonly onEnterCompleted: AnhaCoreEvent<EventArgs>;
    public readonly onExitCompleted: AnhaCoreEvent<EventArgs>;

    constructor(config: SceneConfig) {
        this.tag = config.tag;
        this.predecessorSettings = config.predecessorSettings;
        this.bgColor = new Color(config.bgColor || RGBA.TRANSPARENT);
        this._tickSpeed = 0.016;
        this.accumulator = 0;
        this.active = false;
        this.initialized = false;
        this.clearScreen = false;
        this.running = false;

        this.onReseting = new AnhaCoreEvent<EventArgs>();
        this.onEnterCompleted = new AnhaCoreEvent<EventArgs>();
        this.onExitCompleted = new AnhaCoreEvent<EventArgs>();
    }

    set tickSpeed(value: number) {
        this._tickSpeed = MathTool.clamp(value, 1, 25);
    }

    public activated(): boolean {
        return this.active;
    }

    public activate(): void {
        this.active = true;
        this.unpause();
        this.onEnter();
    }

    public deactivate(): void {
        this.active = false;
        this.pause();
    }

    public pause() {
        this.running = false;
        this.accumulator = 0;
    }

    public unpause() {
        this.running = true;
    }

    protected overlapCompleted(scene: Scene) {
        let settings = scene.predecessorSettings;

        this.disableInput();

        if (settings.pause) {
            this.pause();
        }

        if (settings.effects) {
            Object.assign(this.transform.effects, settings.effects);
        }
    }

    protected resumeCompleted() {
        this.enableInput();
        this.unpause();
    }

    protected exitCompleted(): void {
        this.enableInput();
        this.onExitCompleted.dispatch(this, null);
    }

    protected enterCompleted(): void {
        this.enableInput();
        this.onEnterCompleted.dispatch(this, null);
    }

    public resetSettings(): void {
        this.unpause();

        Object.assign(this.transform.effects, {
            alpha: 1,
            blur: 1,
        });
    }

    public reset(): void {
        this.onReseting.dispatch(this, null);
        this.onReset();
    }

    public enableInput() {
        let children = this.children.all();

        children.forEach((child) => {
            child.listenToInput();
        });

        this.acceptInput = true;
    }

    public disableInput() {
        let children = this.children.all();

        children.forEach((child) => {
            child.ignoreInput();
        });

        this.acceptInput = false;
    }

    public resize(width: number, height: number) {

        this.transform.width = width;
        this.transform.height = height;

        //this.buffer.resize(width, height);
    }

    protected sortChildren(): void {

        this.children.sort((a: SceneObject, b: SceneObject) => {
            return a.transform.zIndex - b.transform.zIndex;
        });
    }

    public add(child: SceneObject): SceneObject {

        this.children.add(child);

        return child;
    }

    public initialize(stage: Canvas, data?: {[key: string]: any}): void {
        this.initialized = true;

        if (stage.isWebGL) {
            //this.renderer = new SceneRendererGL2D(stage);
        } else {
            this.renderer = new SceneRendererC2D(stage);
        }

        this.transitions = new Tweens();
        this.children = new List<SceneObject>();

        this.transform = new Transform2D({
            width: stage.width,
            height: stage.height,
            effects: {
                opacity: 1
            }
        });

        this.onInitialize(stage, data);
    }

    public update(d: number): void {

        if (this.running) {

            this.accumulator += d;

            if (this.accumulator >= this._tickSpeed) {
                this.accumulator -= d;

                d = this._tickSpeed;

                this.sortChildren();

                this.transitions.update(d);

                let children = this.children.all();

                children.forEach((child) => {
                    child.update(d);
                });
            }
        }
    }

    public render(stage: Canvas): void {

        if (this.clearScreen) {
            this.renderer.clearScreen();
        }

        this.renderer.render(this.children.all(), this.transform, this.bgColor);
    }

    public abstract onInitialize(stage: Canvas, data?: {[key: string]: any}): void;
    public abstract onEnter(): void;
    public abstract onExit(): void;
    public abstract onResume(data?: {[key: string]: any}): void;
    public abstract onOverlap(scene: Scene): void;
    public abstract onReset(): void;
}