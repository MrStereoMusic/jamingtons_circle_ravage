import { Canvas } from '../Rendering/Shared/Canvas';
import { Client } from '../Core/Client';
import { Drawable } from './../Core/Drawable';
import { EventArgs } from '../Events/EventArgs';
import { List } from '../Collection/List';
import { Scene } from './Scene';
import { Updatable } from '../Core/Updatable';

export class SceneActivator implements Drawable, Updatable {

    private client: Client;
    public scenes: List<Scene>;
    private numActiveScenes: number;

    constructor(client: Client, scenes: Array<Scene>) {
        this.scenes = new List<Scene>(scenes);
        this.numActiveScenes = 0;
        this.client = client;
    }

    private sceneByTag(sceneTag: string): Scene {
        let scenes: Array<Scene> = this.scenes.where(s => (s.tag === sceneTag));

        if (scenes.length <= 0) {
            console.error(`SceneActivator.sceneByTag: can't find scene by tag ${sceneTag}`);
        }

        return scenes[0];
    }

    public switchToScene(sceneTag: string): void {
        let i;
        let scene;
        let len = this.numActiveScenes;

        // Exit all currently running scenes... without transition for now
        for (i = 0; i < len; ++i) {
            scene = this.scenes.get(i);

            scene.disableInput();
            this.numActiveScenes = 0;
            scene.deactivate();
        }

        this.pushScene(sceneTag);
    }

    public pushScene(sceneTag: string, data?: { [key: string]: any }): boolean {
        let scene = this.sceneByTag(sceneTag);
        let index = this.scenes.findIndex(scene);

        if (scene.activated()) {
            console.info(`SceneActivator.pushScene: scene with tag "${sceneTag}" is already activated.`);

            return false;
        }

        // hacky fix, only for the game jam
        if (scene.initialized) {
            scene.onReset(); // alsnog ff resetten // TODO: test  scene.reset() // to get event
        }
        let client = this.client;

        scene.initialize(this.client.stage, data);
        scene.resize(client.resolution.x, client.resolution.y);
        // }

        if (this.numActiveScenes + 1 > 1) {
            let topScene = this.peek();

            topScene.disableInput();
            topScene.onOverlap(scene);
        }

        scene.disableInput();
        scene.activate();

        this.numActiveScenes++;
        this.scenes.swap(index, this.numActiveScenes - 1);

        scene.onExitCompleted.subscribe(this, this.onSceneExited);

        return true;
    }

    public popScene(data?: { [key: string]: any }): void {
        let scene = this.peek();
        let preScene = this.scenes.get(Math.max(this.numActiveScenes - 2, 0));

        if (this.numActiveScenes >= 2) {
            preScene.resetSettings();
            preScene.onResume(data);
        }

        scene.disableInput();
        scene.onExit();
    }

    public onSceneExited(scene: Scene, args: EventArgs) {
        let index = this.scenes.findIndex(scene);

        this.scenes.swap(index, this.numActiveScenes - 1);
        this.numActiveScenes = Math.max(this.numActiveScenes - 1, 0);

        scene.deactivate();
        scene.onExitCompleted.unsubscribe(this);
    }

    private peek(): Scene {
        let index = Math.max(this.numActiveScenes - 1, 0);

        return this.scenes.get(index);
    }

    public update(d: number): void {
        let scenes = this.scenes.all();
        let i;
        let scene;

        for (i = 0; i < this.numActiveScenes; ++i) {
            scenes[i].update(d);
        }
    }

    public render(stage: Canvas): void {
        let scenes = this.scenes.all();
        let lowestScene = scenes[0];
        let i;
        let scene;

        lowestScene.clearScreen = true;

        for (i = 0; i < this.numActiveScenes; ++i) {
            scenes[i].render(stage);
        }

        lowestScene.clearScreen = false;
    }
}