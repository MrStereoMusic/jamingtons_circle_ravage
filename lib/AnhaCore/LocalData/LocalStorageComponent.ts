import { Base64 } from './../Conversion/Base64';

export class LocalStorageComponent {

    private encoder: Base64;

    constructor() {
        this.encoder = new Base64();
    }

    public save(key: string, item: any): boolean {

        try {
            localStorage.setItem(key, JSON.stringify(item));

            return true;

        } catch (e) {
            console.warn('LocalStorageComponent.save: save not succeeded, key: ' + key);
            console.warn(e);

            return false;
        }

    }

    public delete(key: string): void {
        localStorage.removeItem(key);
    }

    public receive(key: string): {} {
        let data: any;

        try {

            let item = localStorage.getItem(key);

            data = JSON.parse(item);

        } catch (e) {
            console.warn('LocalStorageComponent: could not load item with key ' + key + ' with following Exception: ');
            console.warn(e);

            data = {};
        }

        return data;
    }

}