import { LocalStorageComponent } from './LocalStorageComponent';

export class LocalData {

    private dataKey: string;
    private data: { [key: string]: any };
    private storage: LocalStorageComponent;

    constructor(dataKey: string) {
        this.dataKey = dataKey;
        this.storage = new LocalStorageComponent();
        this.data = null;

        let possibleData = this.storage.receive(this.dataKey);
        
        if (possibleData) {
            this.data = possibleData;
        }
    }

    public hasData(): boolean {
        return Boolean(this.storage.receive(this.dataKey));
    }

    public addSaveData(key: string, data: { [key: string]: any }): void {

        if (!this.data) {
            this.data = {};
        }

        this.data[key] = data;

        this.storage.save(this.dataKey, this.data);
    }

    public getData(): { [key: string]: any } {

        if (!this.data) {
            this.data = this.storage.receive(this.dataKey) || {};
        }

        return this.data;
    }

    public eraseData(): void {
        this.storage.delete(this.dataKey);
        this.data = null;
    }

    public getSavePart(key): { [key: string]: any } {
        let part = {};

        if (this.data) {
            part = this.data[key];

            if (!part) {
                console.warn('Storage.getSavePart: could not load part with key: ' + key);

                part = {};
            }
        }

        return part;
    }

}