import { Axis } from './Axis.enum';
import { Entity } from '../../World/Entity';
import { Side } from './Side.enum';

export class BroadphaseTestResult {

    private _collider: Entity;
    private _withCollider: Entity;
    private _penetration: number;
    private _side: Side;
    private _oppositeSide: Side;
    private _axis: Axis;

    get collider(): Entity {
        return this._collider;
    }

    get withCollider(): Entity {
        return this._withCollider;
    }

    get penetration(): number {
        return this._penetration;
    }

    get side(): Side {
        return this._side;
    }

    get oppositeSide(): Side {
        return this._oppositeSide;
    }

    get axis(): Axis {
        return this._axis;
    }

    public Write(collider: Entity, withCollider: Entity, penetration: number, side: Side, oppositeSide: Side, axis: Axis): void {
        this._collider = collider;
        this._withCollider = withCollider;
        this._penetration = penetration;
        this._side = side;
        this._oppositeSide = oppositeSide;
        this._axis = axis;
    }
}
