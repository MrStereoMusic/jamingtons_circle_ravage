import { Axis } from './Axis.enum';
import { BoundingBox } from '../BoundingBox';
import { BroadphaseTestResult } from './BroadphaseTestResult';
import { Entity } from '../../World/Entity';
import { Side } from './Side.enum';

export class BroadphaseTest {

    private lastResult: BroadphaseTestResult;
    private penetrations: Array<number>;

    constructor() {
        this.lastResult = new BroadphaseTestResult();
        this.penetrations = [];
    }

    get result(): BroadphaseTestResult {
        return this.lastResult;
    }

    public testAABB(entityA: Entity, entityB: Entity, boundsA: BoundingBox, boundsB: BoundingBox, axis: Axis): boolean {

        if (boundsA.intersects(boundsB)) {

            this.penetrations[0] = Math.abs(boundsA.min.y - boundsB.max.y);
            this.penetrations[1] = Math.abs(boundsB.min.x - boundsA.max.x);
            this.penetrations[2] = Math.abs(boundsA.max.y - boundsB.min.y);
            this.penetrations[3] = Math.abs(boundsA.min.x - boundsB.max.x);

            let penetration = Math.min.apply(null, this.penetrations);

            let side = <Side>this.penetrations.indexOf(penetration);
            let oppositeSide = this.invertSide(side);

            this.lastResult.Write(entityA, entityB, penetration, side, oppositeSide, axis);

            return true;
        }

        return false;
    }

    private invertSide(side: Side): Side {
        let inversion = (<number>side + 2) % 4;

        return <Side>inversion;
    }
}