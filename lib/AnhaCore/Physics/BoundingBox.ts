import { BoundingBoxConfig } from '../Config/BoundingBoxConfig';
import { Padding } from './../UI/Padding';
import { Vector2 } from './Vector2';

export class BoundingBox {

    private minPoint: Vector2;
    private maxPoint: Vector2;
    private padding: Padding;

    constructor(config: BoundingBoxConfig) {
        this.padding = new Padding(config.padding || {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0
        });

        this.minPoint = new Vector2(config.min.x + this.padding.left, config.min.y + this.padding.top);
        this.maxPoint = new Vector2(config.max.x - this.padding.right, config.max.y - this.padding.bottom);
    }

    get area(): number {
        return (this.magnitudeX() * this.magnitudeY());
    }

    get min(): Vector2 {
        return this.minPoint;
    }

    get max(): Vector2 {
        return this.maxPoint;
    }

    get center(): Vector2 {
        return this.minPoint.midPoint(this.maxPoint);
    }

    get centerX(): number {
        return this.minPoint.midPointX(this.maxPoint);
    }

    get centerY(): number {
        return this.minPoint.midPointY(this.maxPoint);
    }

    public magnitudeX(): number {
        return (this.maxPoint.x - this.minPoint.x);
    }

    public magnitudeY(): number {
        return (this.maxPoint.y - this.minPoint.y);
    }

    public sync(baseX: number, baseY: number, width?: number, height?: number): void {
        this.minPoint.x = baseX + this.padding.left;
        this.minPoint.y = baseY + this.padding.top;

        width = width || this.magnitudeX();
        height = height || this.magnitudeY();

        this.maxPoint.x = baseX + width - this.padding.right;
        this.maxPoint.y = baseY + height - this.padding.bottom;
    }

    public intersects(bounds: BoundingBox): boolean {

        return (
            this.minPoint.x < bounds.max.x &&
            this.maxPoint.x > bounds.min.x &&
            this.minPoint.y < bounds.max.y &&
            this.maxPoint.y > bounds.min.y
        );

    }

}