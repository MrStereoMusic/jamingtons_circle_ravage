export class Vector2 {

    public x: number;
    public y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    public reset(): void {
        this.x = 0;
        this.x = 0;
    }

    public normalize(): Vector2 {
        let len = this.length();

        this.x /= len;
        this.y /= len;

        return this;
    }

    public length(): number {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    public magnitude(): number {
        return this.length();
    }

    public clone(): Vector2 {
        return new Vector2(this.x, this.y);
    }

    public add(vector: Vector2): Vector2 {
        this.x += vector.x;
        this.y += vector.y;

        return this;
    }

    public subtract(a: Vector2, b: Vector2): Vector2 {
        return new Vector2(a.x - b.x, a.y - b.y);
    }

    public scale(scale: number): Vector2 {
        this.x *= scale;
        this.y *= scale;

        return this;
    }

    public invert(): Vector2 {
        this.x *= -1;
        this.y *= -1;

        return this;
    }

    public midPoint(vector: Vector2): Vector2 {
        return new Vector2((this.x + vector.x) / 2, (this.y + vector.y) / 2);
    }

    public midPointX(vector: Vector2): number {
        return Math.round((this.x + vector.x) / 2);
    }

    public midPointY(vector: Vector2): number {
        return Math.round((this.y + vector.y) / 2);
    }

    public distance(vector: Vector2): number {

        let v1 = this.x - vector.x,
            v2 = this.y - vector.y;

        return Math.sqrt((v1 * v1) + (v2 * v2));
    }

   	public cross(vector: Vector2): number {
    	return this.x * vector.x - this.y * vector.y;
    }

    public dot(vector: Vector2): number {
    	return this.x * vector.x + this.y * vector.y;
    }
}