export class MathTool {

    public static isNumber(value: number): boolean {
        return (typeof value == 'number') && !isNaN(value);
    }

    public static clamp(value: number, min: number, max: number): number {
        return Math.min(Math.max(value, min), max);
    }

    public static lerp(from: number, to: number, time: number): number {
        return (1 - time) * from + time * to;
    }

    public static rand(min: number, max: number): number {
        return Math.floor(min + (Math.random() * max));
    }

    public static toInt(value: string): number {
        return parseInt(value);
    }

    public static invertInt(value: number): number {
        return value * -1;
    }

    public static degToRad(value: number): number {
        return value * (Math.PI / 180);
    }

    public static radToDeg(value: number): number {
        return value / (Math.PI / 180);
    }

}