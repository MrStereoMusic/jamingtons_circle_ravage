export class Transitions {

    public static linear(elapsed: number, from: number, progress: number, duration: number): number {
        return progress * elapsed / duration + from;
    }

    public static ease(elapsed: number, from: number, progress: number, duration: number): number {
        elapsed /= duration / 2;

        if (elapsed < 1) return progress / 2 * elapsed * elapsed + from;

        elapsed--;

        return -progress / 2 * (elapsed * (elapsed - 2) - 1) + from;
    }

    public static bounceOut(elapsed: number, from: number, progress: number, duration: number): number {
        return (elapsed /= duration) < 1 / 2.75 ? progress * 7.5625 * elapsed * elapsed + from : elapsed < 2 / 2.75 ? progress * (7.5625 * ( elapsed -= 1.5 / 2.75) * elapsed + .75) + from : elapsed < 2.5 / 2.75 ? progress * (7.5625 * (elapsed -= 2.25 / 2.75) * elapsed + .9375) + from : progress * (7.5625 * (elapsed -= 2.625 / 2.75) * elapsed + .984375) + from;
    }
}