export interface TransitionDelegate {
    (elapsed: number, from: number, progress: number, duration: number): number
}