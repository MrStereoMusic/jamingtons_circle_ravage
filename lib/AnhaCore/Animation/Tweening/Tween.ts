import { Action } from '../../Core/Action';
import { AnhaCoreEvent } from '../../Events/AnhaCoreEvent';
import { EventArgs } from '../../Events/EventArgs';
import { TransitionDelegate } from './Transition/TransitionDelegate';
import { Transitions } from './Transition/Transitions';
import { TweenConfig } from '../../Config/TweenConfig';
import { TweenValueChange } from './TweenValueChange';
import { Updatable } from '../../Core/Updatable';

export class Tween implements Updatable {

    private properties: Array<TweenValueChange>;
    private elapsed: number;
    private delay: number;
    private initDelay: number;
    private duration: number;
    private easing: TransitionDelegate;
    private callback: Action;
    public readonly loop: boolean;
    private init: Action;
    public timesLooped: number;
    public running: boolean;

    public readonly onFinished: AnhaCoreEvent<EventArgs>;

    constructor(config: TweenConfig) {

        config = Object.assign(config, {
            elapsed: 0.0,
            delay: 0,
            duration: 0,
            callback: null,
            easing: Transitions.ease,
            loop: false,
            init: null,
            timesLooped: 0,
            ...config
        });

        this.properties = config.properties;
        this.delay = config.delay;
        this.duration = config.duration;
        this.callback = config.callback;
        this.easing = config.easing;
        this.loop = config.loop; 
        this.init = config.init;
        this.initDelay = config.delay || 0;

        this.onFinished = new AnhaCoreEvent<EventArgs>();

        this.timesLooped = 0;
        this.elapsed = 0;
    }

    get finished(): boolean {
        return (this.elapsed > this.duration);
    }

    public invokeCallback(): void {

        if (this.callback) {
            this.callback();
        }

    }

    public onInitialize(): void {

        if (this.init && this.timesLooped == 0) {
            this.init();
        }

    }

    public reset(): void {
        this.delay = this.initDelay;
        this.elapsed = 0;
        this.running = false;
    }

    public update(d: number): void {

        this.delay -= d;

        if (this.elapsed > this.delay) {
            let prop;
            let to;

            this.elapsed += d;

            for (prop of this.properties) {
                to = this.easing.call(Transitions, this.elapsed, prop.from, prop.to - prop.from, this.duration);
				prop.change(to);
            }

            if (this.finished) {
                this.onFinished.dispatch(this, {});
            }
        }
    }
}