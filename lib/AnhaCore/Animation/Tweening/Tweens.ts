import { List } from '../../Collection/List';
import { Tween } from './Tween';
import { TweenConfig } from '../../Config/TweenConfig';
import { Updatable } from '../../Core/Updatable';

export class Tweens implements Updatable {

    private animations: Array<Tween>;
    private endedTweenIndices: List<number>;
    private busy: boolean;
    private running: boolean;

    constructor() {
        this.animations = [];
        this.endedTweenIndices = new List<number>();
        this.busy = false;
        this.running = true;
    }

    public animating(): boolean {
        return this.busy;
    }

    public pause(): void {
        this.running = false;
    }

    public unpause(): void {
        this.running = true;
    }

    public add(conf: TweenConfig): void {
        this.animations.push(new Tween(conf));
    }

    public reset(): void {
        let tween;
        let len = this.animations.length;
        let i;

        for (i = 0; i < len; ++i) {
            tween = this.animations[i];
            tween.reset();
        }
    }

    public clear(): void {
        this.disposeFinishedTweens();
    }

    private disposeFinishedTweens(): void {
        let numFinishedTweens = this.endedTweenIndices.length;
        let endedTweenIndices = this.endedTweenIndices.all();

        this.busy = false;

        endedTweenIndices.forEach(i => {
            this.animations[i].invokeCallback();
        });

		let tween;
		// cache lengte array = fout

        let i;
        for (i = 0; i < numFinishedTweens; ++i) {
            tween = this.animations[endedTweenIndices[i]];

            if (tween) {

                if (tween.loop) {
                    this.busy = true;
                    tween.reset();
                } else {
                    this.animations.splice(i, 1);
                }

            }
        }

        this.endedTweenIndices.clear();
    }

    public update(d: number): void {

        if (!this.running) {
            return;
        }

        let tween;
        let i = 0;
        for (i = 0; i < this.animations.length; ++i) {
            tween = this.animations[i];

            if (!tween.running) {
                tween.onInitialize();
                tween.running = true;
            }

            if (tween.finished) {
                this.endedTweenIndices.add(i);
            } else {
                this.busy = true;
                tween.update(d);
            }
        }

        this.disposeFinishedTweens();
    }
}