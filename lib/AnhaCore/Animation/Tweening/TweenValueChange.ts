export type TweenValueChange = {
    from: number;
    to: number;
    change: (to: number) => void;
};