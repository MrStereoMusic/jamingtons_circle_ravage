import { Base64 } from '../../Conversion/Base64';
import { CanvasConfig } from '../../Config/CanvasConfig';
import { MathTool } from '../../Tool/MathTool';

export class Canvas {

    private name: string;

    private _width: number;
    private _height: number;

    private _domElement: HTMLCanvasElement;
    public ctx: CanvasRenderingContext2D | WebGLRenderingContext;

    public readonly isWebGL;

    constructor(config: CanvasConfig) {
        let element = document.createElement("canvas");

        this.name = (new Base64()).encode(`${config.name}-${MathTool.rand(0, 999999)}`);

        let context = "2d";
        this.isWebGL = false;

        if (config.enableWebGL && element.getContext("webgl")) {
            context = "webgl";
            this.isWebGL = true;
        }

        this._domElement = element;
        this.ctx = this._domElement.getContext(context);

        this._width = config.width;
        this._height = config.height;

        element.id = this.name;
        element.width = this._width;
        element.height = this._height;
    }

    get width(): number {
        return this._width;
    }

    set width(width: number) {
        this._width = width;
        this._domElement.width = this._width;
    }

    get height(): number {
        return this._height;
    }

    set height(height: number) {
        this._height = height;
        this._domElement.height = this._height;
    }

    get centerX(): number {
        return (this._width * .5);
    }

    get centerY(): number {
        return (this._height * .5);
    }

    get domElement(): HTMLCanvasElement {
        return this._domElement;
    }

    get context(): CanvasRenderingContext2D | WebGLRenderingContext {
        return this.ctx;
    }
}