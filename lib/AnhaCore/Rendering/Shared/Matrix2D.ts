export class Matrix2D {

    public readonly current: Array<number>;
    private previousCurrent: Array<number>;

    private identity: Array<number>;
    private cache: Array<number>;

    constructor() {
        this.identity = [
            1, 0, 0,
            0, 1, 0,
            0, 0, 1,
        ];
        this.current = [];
        this.cache = [];
        this.previousCurrent = [];

        this.copy(this.identity, this.current);
    }

    private copy(from: Array<number>, to: Array<number>): void {
        let i;
        let len = from.length;

        for (i = 0; i < len; ++i) {
            to[i] = from[i];
        }
    }

    public save(): void {
        this.copy(this.current, this.previousCurrent);
    }

    public restore(): void {

        if (this.previousCurrent[0] != null) {
            this.copy(this.previousCurrent, this.current);

            for (let i = 0; i < this.previousCurrent.length; ++i) {
                this.previousCurrent[i] = null;
            }
        }
    }

    public project(width: number, height: number): Array<number> {
        return this.assignMatrix(this.current,
            2 / width, 0, 0,
            0, -2 / height, 0,
            -1, 1, 1
        );
    }

    public translate(tx: number, ty: number): Array<number> {
        return this.multiply(
            this.assignMatrix(this.cache,
                1, 0, 0,
                0, 1, 0,
                tx, ty, 1
            )
        );
    }

    public rotate(radians: number): Array<number> {
        var c = Math.cos(radians);
        var s = Math.sin(radians);

        return this.multiply(
            this.assignMatrix(this.cache,
                c, -s, 0,
                s, c, 0,
                0, 0, 1
            )
        );
    }

    public scale(sx: number, sy: number): Array<number> {
        return this.multiply(
            this.assignMatrix(this.cache,
                sx, 0, 0,
                0, sy, 0,
                0, 0, 1
            )
        );
    }

    private assignMatrix(matrix: Array<number>, a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number): Array<number> {

        // looping through arguments doesn't work somehow
        matrix[0] = a;
        matrix[1] = b;
        matrix[2] = c;
        matrix[3] = d;
        matrix[4] = e;
        matrix[5] = f;
        matrix[6] = g;
        matrix[7] = h;
        matrix[8] = i;

        return matrix;
    }

    public multiply(b: Array<number>): Array<number> {
        var a = this.current;

        var a00 = a[0];
        var a01 = a[1];
        var a02 = a[2];
        var a10 = a[3];
        var a11 = a[4];
        var a12 = a[5];
        var a20 = a[6];
        var a21 = a[7];
        var a22 = a[8];

        a[0] = (b[0] * a00 + b[1] * a10 + b[2] * a20);
        a[1] = (b[0] * a01 + b[1] * a11 + b[2] * a21);
        a[2] = (b[0] * a02 + b[1] * a12 + b[2] * a22);

        a[3] = (b[3] * a00 + b[4] * a10 + b[5] * a20);
        a[4] = (b[3] * a01 + b[4] * a11 + b[5] * a21);
        a[5] = (b[3] * a02 + b[4] * a12 + b[5] * a22);

        a[6] = (b[6] * a00 + b[7] * a10 + b[8] * a20);
        a[7] = (b[6] * a01 + b[7] * a11 + b[8] * a21);
        a[8] = (b[6] * a02 + b[7] * a12 + b[8] * a22);

        return this.current;
    }
}