import { Transform2D } from '../../Component/UI/Transform2D';
import { View2D } from '../../Component/UI/View2D';

export interface Renderer {
    begin(object: View2D, transform: Transform2D): void;
    end(transform: Transform2D): void;
    clearScreen(): void;
    setBackgroundColor(r: number, g: number, b: number, a: number): void;
    render(...args: any[]): void;
};