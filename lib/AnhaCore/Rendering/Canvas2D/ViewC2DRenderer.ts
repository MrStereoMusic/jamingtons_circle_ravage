import { Canvas } from '../Shared/Canvas';
import { Canvas2DRenderer } from './Canvas2DRenderer';
import { MathTool } from '../../Tool/MathTool';
import { Renderer } from '../Shared/Renderer';
import { Transform2D } from '../../Component/UI/Transform2D';
import { View2D } from '../../Component/UI/View2D';

export abstract class ViewC2DRenderer extends Canvas2DRenderer {

    public render(object: View2D, transform: Transform2D): void {
        let effects = transform.effects;

        if (effects.opacity == 0) {
            return;
        }

        this.mirror(effects.mirrorX, transform, "x");
        this.mirror(effects.mirrorY, transform, "y");

        this.begin(object, transform);
        this.renderOperation(object, transform);
        this.end(transform);
    }

    abstract renderOperation(object: View2D, transform: Transform2D): void;
}