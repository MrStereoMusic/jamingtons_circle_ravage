import { Canvas } from '../Shared/Canvas';
import { Canvas2DRenderer } from './Canvas2DRenderer';
import { Color } from '../../UI/Color';
import { SceneObject } from '../../Scene/SceneObject';
import { Transform2D } from '../../Component/UI/Transform2D';
import { View2D } from '../../Component/UI/View2D';

export class SceneRendererC2D extends Canvas2DRenderer {

    private buffer: Canvas;

    constructor(stage: Canvas) {
        super(stage);

        this.buffer = new Canvas({
            name: "sceneBuffer",
            width: stage.width,
            height: stage.height
        });
    }

    public render(sceneChildren: Array<SceneObject>, sceneTransform: Transform2D, bgColor: Color): void {
        let stage = this.stage;
        let oldCtx = this.ctx;
        let renderer;

        this.begin(null, sceneTransform);

        let bufferCtx = <CanvasRenderingContext2D>this.buffer.context;

        this.ctx = bufferCtx;

        this.clearScreen();

        this.setBackgroundColor(bgColor.r, bgColor.g, bgColor.b, bgColor.a);

        let stageOldCtx;
        stageOldCtx = stage.ctx;
        stage.ctx = bufferCtx;

        sceneChildren.forEach((child: SceneObject) => {
            renderer = child.renderer;

            if (renderer) {
                renderer.setContext(bufferCtx);
                child.render(stage);
                renderer.setContext(oldCtx);
            } else {
                child.render(stage);
            }

        });

        stage.ctx = stageOldCtx;
        this.ctx = oldCtx;

        oldCtx.drawImage(this.buffer.domElement, 0, 0, stage.width, stage.height, 0, 0, stage.width, stage.height);

        this.end(sceneTransform);
    }
}