import { Circle } from '../../Component/UI/Circle';
import { MathTool } from '../../Tool/MathTool';
import { Rectangle } from '../../Component/UI/Rectangle';
import { Transform2D } from '../../Component/UI/Transform2D';
import { ViewC2DRenderer } from './ViewC2DRenderer';

export class CircleRendererC2D extends ViewC2DRenderer {

    public renderOperation(object: Circle, transform: Transform2D) {
        let color = object.color;
        let ctx = this.ctx;
        let pi = 2 * Math.PI;

        // HACKY, HAHA
        ctx.restore();
        ctx.globalAlpha = transform.effects.opacity;

        ctx.beginPath();
        ctx.arc(transform.position.x, transform.position.y, object.radius, 0, pi);
        ctx.fillStyle = `rgba(${color.r},${color.g},${color.b},${color.a})`;
        ctx.fill();
        
        ctx.globalAlpha = 1;
    }
}