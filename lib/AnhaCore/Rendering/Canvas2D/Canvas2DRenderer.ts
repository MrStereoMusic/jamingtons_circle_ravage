import { Canvas } from '../Shared/Canvas';
import { MathTool } from '../../Tool/MathTool';
import { Renderer } from '../Shared/Renderer';
import { Transform2D } from '../../Component/UI/Transform2D';
import { View2D } from '../../Component/UI/View2D';

export abstract class Canvas2DRenderer implements Renderer {

    protected stage: Canvas;
    public ctx: CanvasRenderingContext2D;

    constructor(stage: Canvas) {
        this.stage = stage;
        this.ctx = <CanvasRenderingContext2D>stage.context;
    }

    // TODO: probably in Transform2D class.
    protected mirror(enabled: boolean, transform: Transform2D, axis: string): void {

        if (enabled) {

            if (transform.scale[axis] > 0) {
                transform.scale[axis] *= -1;
                transform.origin[axis] *= -1;
            }

        } else {
            transform.scale[axis] = Math.abs(transform.scale[axis]);
            transform.origin[axis] = Math.abs(transform.origin[axis]);
        }

    }

    public setBackgroundColor(r: number, g: number, b: number, a: number): void {
        let ctx = this.ctx;
        let stage = this.stage;

        ctx.fillStyle = `rgba(${r},${g},${b},${a})`;
        ctx.fillRect(0, 0, stage.width, stage.height);
    }

    public setContext(ctx: CanvasRenderingContext2D): void {
        this.ctx = ctx;
    }

    public begin(object: View2D, transform: Transform2D): void {
        let effects = transform.effects;
        let rotation;
        let ctx = this.ctx;

        ctx.save();

        rotation = MathTool.degToRad(transform.angle);

        ctx.translate(
            transform.position.x + Math.round(transform.translation.x) + transform.origin.x,
            transform.position.y + Math.round(transform.translation.y) + transform.origin.y
        );
        ctx.rotate(rotation);
        ctx.scale(transform.scale.x, transform.scale.y);
        ctx.translate(-transform.origin.x, -transform.origin.y);

        ctx.globalAlpha = effects.opacity;
    }

    public end(transform: Transform2D): void {
        this.ctx.restore();
    }

    public clearScreen(): void {
        this.ctx.clearRect(0, 0, this.stage.width, this.stage.height);
    }

    abstract render(...args: any[]): void;
}