import { Sprite } from '../../Component/UI/Sprite';
import { Transform2D } from '../../Component/UI/Transform2D';
import { ViewC2DRenderer } from './ViewC2DRenderer';

export class SpriteRendererC2D extends ViewC2DRenderer {

    public renderOperation(object: Sprite, transform: Transform2D) {
        this.ctx.drawImage(object.image, object.frame.x, object.frame.y, transform.width, transform.height, 0, 0, transform.width, transform.height);
    }
}