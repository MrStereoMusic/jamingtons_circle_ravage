import { Canvas } from '../Shared/Canvas';
import { Canvas2DRenderer } from './Canvas2DRenderer';
import { Entity } from '../../World/Entity';
import { Transform2D } from '../../Component/UI/Transform2D';
import { Vector2 } from '../../Physics/Vector2';
import { View2D } from '../../Component/UI/View2D';

export class WorldRendererC2D extends Canvas2DRenderer {

    constructor(stage: Canvas) {
        super(stage);
    }

    public render(entities: Array<Entity>, transform: Transform2D) {
        let view;
        let viewTransform;
        let ctx = this.ctx;

        this.begin(null, transform);

        entities.forEach(entity => {
            view = entity.getComponent<View2D>("view2d");
            viewTransform = entity.getComponent<Transform2D>("transform2d");
            let oldCtx = view.renderer.ctx;

            view.renderer.setContext(this.ctx);
            view.renderer.render(view, viewTransform);
            view.renderer.setContext(oldCtx);
        });

        this.end(transform);
    }
}