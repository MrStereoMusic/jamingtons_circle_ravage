import { Rectangle } from '../../Component/UI/Rectangle';
import { Transform2D } from '../../Component/UI/Transform2D';
import { ViewC2DRenderer } from './ViewC2DRenderer';

export class RectangleRendererC2D extends ViewC2DRenderer {

    public renderOperation(object: Rectangle, transform: Transform2D) {
        let color = object.color;
        let ctx = this.ctx;

        ctx.fillStyle = `rgba(${color.r},${color.g},${color.b},${color.a})`;
        ctx.fillRect(0, 0, transform.width, transform.height);
    }
}