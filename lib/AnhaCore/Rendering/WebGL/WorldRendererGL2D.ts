// import { Entity } from '../../World/Entity';
// import { Transform2D } from '../../Component/UI/Transform2D';
// import { View2D } from '../../Component/UI/View2D';
// import { ViewGL2DRenderer } from './ViewGL2DRenderer';
// import { WebGL2DRenderer } from './WebGL2DRenderer';

// export class WorldRendererGL2D extends WebGL2DRenderer {

//     public render(entities: Array<Entity>, cameraTransform: Transform2D) {
//         let view;
//         let viewTransform;
//         let stage = this.stage;
//         let ctx = this.ctx;
//         let computedCameraMatrix = cameraTransform.computeMatrix(stage.width, stage.height);

//         entities.forEach(entity => {
//             view = entity.getComponent<View2D>("view2d");
//             viewTransform = entity.getComponent<Transform2D>("transform2d");

//             // TODO: find the relative matrix problem, camera scaling/rotation etc is impossible this way
//             // problem is: position is multiplied by 0. position == translation
//             viewTransform.translation.x = cameraTransform.translation.x;
//             viewTransform.translation.y = cameraTransform.translation.y;

//             //(<ViewGL2DRenderer>view.renderer).setRelativeMatrix(computedCameraMatrix);
//             view.renderer.render(view, viewTransform);
//         });
//     }

//     protected onLocalizeVariables() {
//         //
//     }

//     protected onInitializeBuffers(): void {
//         //
//     }
// }