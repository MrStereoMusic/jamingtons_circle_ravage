// import { Canvas } from '../Shared/Canvas';
// import { Sprite } from '../../Component/UI/Sprite';
// import { textureFragmentShader } from './Shader/TextureFragmentShader';
// import { Transform2D } from '../../Component/UI/Transform2D';
// import { vertexShader } from './Shader/VertexShader';
// import { ViewGL2DRenderer } from './ViewGL2DRenderer';

// export class SpriteRendererGL2D extends ViewGL2DRenderer {

//     private texture: WebGLTexture;
//     private texCoordBuffer: WebGLBuffer;

//     private texSizeLocation: WebGLUniformLocation;
//     private texCoordLocation: number;

//     private texCoordVertices: Float32Array;

//     constructor(stage: Canvas) {
//         super(stage);

//         this.texCoordVertices = new Float32Array([
//             0, 0,
//             0, 0,
//             0, 0,
//             0, 0,
//             0, 0,
//             0, 0
//         ]);
        
//         this.initialize(vertexShader, textureFragmentShader);

//         let gl = this.ctx;

//         this.texture = gl.createTexture();

//         gl.activeTexture(gl.TEXTURE0);
//         gl.bindTexture(gl.TEXTURE_2D, this.texture);

//         gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

//         gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
//         gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
//         gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
//         gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
//     }

//     protected onInitializeBuffers(object: Sprite, transform: Transform2D): void {
//         let gl = this.ctx;
//         let image = object.image;

//         this.texCoordBuffer = gl.createBuffer();
//         this.setTexCoords(object, transform);
//     }

//     protected onLocalizeVariables() {
//         let gl = this.ctx;

//         this.texSizeLocation = gl.getUniformLocation(this.program, "u_textureSize");
//         this.texCoordLocation = gl.getAttribLocation(this.program, "a_texCoord");
//     }

//     private setTexCoords(object: Sprite, transform: Transform2D): void {
//         let gl = this.ctx;
//         let image = object.image;

//         gl.bindBuffer(gl.ARRAY_BUFFER, this.texCoordBuffer);

//         let x1 = object.frame.x / image.width;
//         let y1 = 1.0 - (object.frame.y / image.height);
//         let x2 = ((object.frame.x + transform.width) / image.width);
//         let y2 = 1.0 - ((object.frame.y + transform.height) / image.height);

//         this.texCoordVertices[0] = x1;
//         this.texCoordVertices[1] = y1;
//         this.texCoordVertices[2] = x2;
//         this.texCoordVertices[3] = y1;
//         this.texCoordVertices[4] = x1;
//         this.texCoordVertices[5] = y2;
//         this.texCoordVertices[6] = x1;
//         this.texCoordVertices[7] = y2;
//         this.texCoordVertices[8] = x2;
//         this.texCoordVertices[9] = y1;
//         this.texCoordVertices[10] = x2;
//         this.texCoordVertices[11] = y2;

//         gl.bufferData(gl.ARRAY_BUFFER, this.texCoordVertices, gl.STATIC_DRAW);
//     }

//     protected setArbitraryVariables(object: Sprite, transform: Transform2D) {
//         let gl = this.ctx;
//         let image = object.image;

//         this.setTexCoords(object, transform);

//         gl.enableVertexAttribArray(this.texCoordLocation);
//         gl.bindBuffer(gl.ARRAY_BUFFER, this.texCoordBuffer);
//         gl.vertexAttribPointer(this.texCoordLocation, 2, gl.FLOAT, false, 0, 0);

//         gl.uniform2f(this.texSizeLocation, transform.width, transform.height);

//         gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
//     }
// }
