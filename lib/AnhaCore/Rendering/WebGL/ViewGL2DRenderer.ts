// import { Transform2D } from '../../Component/UI/Transform2D';
// import { View2D } from '../../Component/UI/View2D';
// import { WebGL2DRenderer } from './WebGL2DRenderer';

// export abstract class ViewGL2DRenderer extends WebGL2DRenderer {

//     public render(object: View2D, transform: Transform2D): void {
//         let gl = this.ctx;
//         let iterations = ~~(this.vertices.length * .5);

//         this.begin(object, transform);

//         this.setArbitraryVariables(object, transform);

//         gl.drawArrays(gl.TRIANGLES, 0, iterations);

//         this.end(transform);
//     }

//     protected abstract setArbitraryVariables(object: View2D, transform: Transform2D);
// }