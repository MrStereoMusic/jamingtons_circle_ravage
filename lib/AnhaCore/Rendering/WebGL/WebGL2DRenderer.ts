// import { Canvas } from '../Shared/Canvas';
// import { MathTool } from '../../Tool/MathTool';
// import { Matrix2D } from '../Shared/Matrix2D';
// import { Renderer } from '../Shared/Renderer';
// import { textureFragmentShader } from './Shader/TextureFragmentShader';
// import { Transform2D } from '../../Component/UI/Transform2D';
// import { View2D } from '../../Component/UI/View2D';

// export abstract class WebGL2DRenderer implements Renderer {

//     protected stage: Canvas;
//     protected ctx: WebGLRenderingContext;

//     protected program: WebGLProgram;

//     protected positionBuffer: WebGLBuffer;
//     protected vertices: Float32Array;

//     protected aPositionLocation: number;
//     protected uMatrixLocation: WebGLUniformLocation;
//     protected uAlphaLocation: WebGLUniformLocation;

//     private buffersInitialized: boolean;

//     private relativeMatrix: Matrix2D;

//     constructor(stage: Canvas) {
//         this.stage = stage;
//         this.ctx = <WebGLRenderingContext>stage.context;

//         this.buffersInitialized = false;

//         this.vertices = new Float32Array([
//             0, 0,
//             0, 0,
//             0, 0,
//             0, 0,
//             0, 0,
//             0, 0
//         ]);
//     }

//     protected createShader(type: number, source: string) {
//         let gl = this.ctx;
//         let shader = gl.createShader(type);

//         gl.shaderSource(shader, source);
//         gl.compileShader(shader);

//         var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);

//         if (success) {
//             return shader;
//         }

//         console.log(gl.getShaderInfoLog(shader));
//         gl.deleteShader(shader);
//     }

//     protected createProgram(vertexShader: WebGLShader, fragmentShader: WebGLShader) {
//         let gl = this.ctx;
//         let program = gl.createProgram();

//         gl.attachShader(program, vertexShader);
//         gl.attachShader(program, fragmentShader);

//         gl.linkProgram(program);

//         let success = gl.getProgramParameter(program, gl.LINK_STATUS);

//         if (success) {
//             return program;
//         }

//         console.log(gl.getProgramInfoLog(program));
//         gl.deleteProgram(program);
//     }

//     protected enableBlending(): void {
//         let gl = this.ctx;

//         gl.enable(gl.BLEND);
//         gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
//         gl.disable(gl.DEPTH_TEST);
//     }

//     private localizeVariables(): void {
//         let gl = this.ctx;
//         let program = this.program;

//         this.aPositionLocation = gl.getAttribLocation(program, "a_position");

//         this.uMatrixLocation = gl.getUniformLocation(program, "u_matrix");
//         this.uAlphaLocation = gl.getUniformLocation(program, "u_alpha");

//         this.onLocalizeVariables();
//     }

//     protected initialize(vShader: string, fShader: string): void {
//         let gl = this.ctx;
//         let canvas = gl.canvas;
//         let vertexShader = this.createShader(gl.VERTEX_SHADER, vShader);
//         let fragmentShader = this.createShader(gl.FRAGMENT_SHADER, fShader);

//         this.program = this.createProgram(vertexShader, fragmentShader);

//         this.enableBlending();
//         this.localizeVariables();

//         gl.viewport(0, 0, canvas.width, canvas.height);
//     }

//     public begin(object: View2D, transform: Transform2D): void {
//         let gl = this.ctx;
//         let canvas = gl.canvas;
//         let childMatrix = transform.computeMatrix(canvas.clientWidth, canvas.clientHeight);

//         gl.useProgram(this.program);

//         childMatrix.save();

//         if (this.relativeMatrix) {
//             childMatrix.multiply(this.relativeMatrix.current);
//         } else {
//             this.relativeMatrix = childMatrix;
//         }

//         if (!this.buffersInitialized) {
//             this.initializeBuffers(object, transform);
//             this.buffersInitialized = true;
//         }

//         this.enableVariables(transform);
//     }

//     protected initializeBuffers(object: View2D, transform: Transform2D): void {
//         let gl = this.ctx;

//         this.positionBuffer = gl.createBuffer();
//         gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer);

//         let x2 = transform.width;
//         let y2 = transform.height;

//         this.vertices[2] = this.vertices[8] = this.vertices[10] = x2;
//         this.vertices[5] = this.vertices[7] = this.vertices[11] = y2;

//         gl.bufferData(gl.ARRAY_BUFFER, this.vertices, gl.STATIC_DRAW);

//         this.onInitializeBuffers(object, transform);
//     }

//     public enableVariables(transform: Transform2D): void {
//         let gl = this.ctx;
//         let effects = transform.effects;

//         gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer);

//         gl.enableVertexAttribArray(this.aPositionLocation);
//         gl.vertexAttribPointer(this.aPositionLocation, 2, gl.FLOAT, false, 0, 0);

//         gl.uniform1f(this.uAlphaLocation, effects.opacity);

//         gl.uniformMatrix3fv(this.uMatrixLocation, false, this.relativeMatrix.current);
//     }

//     public setRelativeMatrix(matrix: Matrix2D): void {
//         this.relativeMatrix = matrix;
//     }

//     public end(transform: Transform2D): void {
//         transform.matrix.restore();
//         this.relativeMatrix = null;
//     }

//     public setBackgroundColor(r: number, g: number, b: number, a: number): void {
//         let gl = this.ctx;

//         gl.clearColor(r, g, b, a);
//         gl.clear(gl.COLOR_BUFFER_BIT);
//     }

//     public setContext(ctx: WebGLRenderingContext): void {
//         this.ctx = ctx;
//     }

//     public clearScreen(): void {
//         let gl = this.ctx;
//         let stage = this.stage;

//         gl.viewport(0, 0, stage.width, stage.height);
//         gl.clear(gl.COLOR_BUFFER_BIT);
//     }

//     public abstract render(...args: any[]): void;

//     protected abstract onLocalizeVariables();
//     protected abstract onInitializeBuffers(object: View2D, transform: Transform2D): void;
// }