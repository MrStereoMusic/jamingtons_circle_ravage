// import { Canvas } from '../Shared/Canvas';
// import { Color } from '../../UI/Color';
// import { Origin } from '../../Component/UI/Origin.enum';
// import { Rectangle } from '../../Component/UI/Rectangle';
// import { RGBA } from '../../UI/RGBA';
// import { SceneObject } from '../../Scene/SceneObject';
// import { Sprite } from '../../Component/UI/Sprite';
// import { Transform2D } from '../../Component/UI/Transform2D';
// import { ViewGL2DRenderer } from './ViewGL2DRenderer';
// import { WebGL2DRenderer } from './WebGL2DRenderer';

// export class SceneRendererGL2D extends WebGL2DRenderer {

//     private bgRect: Rectangle;
//     private bgTransform: Transform2D;

//     constructor(stage: Canvas) {
//         super(stage);

//         this.bgRect = new Rectangle({
//             color: RGBA.CORNFLOWERBLUE,
//             stage: stage
//         });

//         this.bgTransform = new Transform2D({
//             width: stage.width,
//             height: stage.height
//         });
//     }

//     public render(sceneChildren: Array<SceneObject>, sceneTransform: Transform2D, bgColor: Color): void {
//         let stage = this.stage;
//         let ctx = this.ctx;
//         let computedMatrix = sceneTransform.computeMatrix(stage.width, stage.height);
//         let bg = this.bgRect;
//         let renderer;

//         bg.color.change(bgColor.r, bgColor.g, bgColor.b, bgColor.a);

//         (<ViewGL2DRenderer>bg.renderer).setRelativeMatrix(computedMatrix);
//         bg.renderer.render(bg, this.bgTransform);

//         sceneChildren.forEach((child: SceneObject) => {
//             (<ViewGL2DRenderer>child.renderer).setRelativeMatrix(computedMatrix);
//             child.render(stage);
//         });
//     }

//     protected onLocalizeVariables() {
//         //
//     }

//     protected onInitializeBuffers(): void {
//         //
//     }
// }