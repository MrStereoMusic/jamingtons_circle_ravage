// export const textureFragmentShader = `
//     precision mediump float;

//     uniform sampler2D u_image;
//     uniform float u_alpha;

//     varying vec2 v_texCoord;

//     void main() {
//         gl_FragColor = texture2D(u_image, v_texCoord) * u_alpha;
//     }
// `;