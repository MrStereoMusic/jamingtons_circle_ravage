// import { Canvas } from '../Shared/Canvas';
// import { colorFragmentShader } from './Shader/ColorFragmentShader';
// import { Rectangle } from '../../Component/UI/Rectangle';
// import { Transform2D } from '../../Component/UI/Transform2D';
// import { vertexShader } from './Shader/VertexShader';
// import { View2D } from '../../Component/UI/View2D';
// import { ViewGL2DRenderer } from './ViewGL2DRenderer';

// export class RectangleRendererGL2D extends ViewGL2DRenderer {

//     private colorLocation: WebGLUniformLocation;

//     constructor(stage: Canvas) {
//         super(stage);

//         this.initialize(vertexShader, colorFragmentShader);
//     }

//     protected onInitializeBuffers(): void {
//         //
//     }

//     protected onLocalizeVariables() {
//         let gl = this.ctx;
        
//         this.colorLocation = gl.getUniformLocation(this.program, "u_color");
//     }

//     protected setArbitraryVariables(object: Rectangle, transform: Transform2D) {
//         let gl = this.ctx;

//         gl.uniform4fv(this.colorLocation, object.color.toWebGLValues());
//     }
// }
