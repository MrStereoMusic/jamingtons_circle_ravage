// import { Canvas } from './Canvas';
// import { Text } from '../Component/UI/Text';
// import { Transform2D } from '../Component/UI/Transform2D';
// import { View2DRenderer } from './View2DRenderer';

// export class TextRenderer extends View2DRenderer {

//     private buffer: Canvas;
//     private buffered: boolean;

//     constructor() {
//         super();

//         this.buffer = new Canvas({
//             name: "textBuffer",
//             width: 0,
//             height: 0
//         });

//         this.buffered = false;
//     }

//     private preRender(object: Text, transform: Transform2D) {
//         let x = 0;
//         let y = 0;
//         let words = object.label.split(' ');
//         let line = "";
//         let n;
//         let testLine;
//         let testWidth;
//         let bufferCtx = this.buffer.context;

//         this.buffer.width = transform.width;
//         this.buffer.height = transform.height;

//         bufferCtx.font = object.font;
//         bufferCtx.fillStyle = object.color;

//         for (n = 0; n < words.length; ++n) {
//             testLine = line + words[n] + " ";
//             testWidth = bufferCtx.measureText(testLine).width;

//             if (testWidth > transform.width && n > 0) {

//                 bufferCtx.fillText(line, x, y);

//                 line = words[n] + " ";
//                 y += object.lineHeight;

//             } else {
//                 line = testLine;
//             }
//         }

//         bufferCtx.fillText(line, x, y);
//     }

//     protected renderOperation(ctx: CanvasRenderingContext2D, object: Text, transform: Transform2D): void {

//         if (!this.buffered) {
//             this.preRender(object, transform);
//             this.buffered = true;
//         }

//         ctx.drawImage(this.buffer.domElement, 0, 0, transform.width, transform.height);
//     }
// }